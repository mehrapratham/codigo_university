import initialState from '../initialState'
import { CREATE_ARTICLE_DATA } from '../../actions/types'
export default (state = initialState.Article, action) => {
 switch (action.type) {
   case CREATE_ARTICLE_DATA:
    return { ...state, createArticleData: action.data }
   default:
    return state
 }
}