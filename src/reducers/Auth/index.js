import initialState from '../initialState'
import { GET_LOGIN_DATA } from '../../actions/types'
export default (state = initialState.Auth, action) => {
 switch (action.type) {
   case GET_LOGIN_DATA:
    return { ...state, authList: action.data }
   default:
    return state
 }
}