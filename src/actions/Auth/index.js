import axios from 'axios'
import { LOGIN_API, AAHDAAR_API, SENDOTP_API, VALIDATE_OTP_API } from '../apiConstant'
import { HAS_ERROR, GET_LOGIN_DATA, GET_AAHDAAR_DATA, GET_OTP_DATA, GET_VALIDATE_OTP_DATA } from '../types'
import {AsyncStorage } from 'react-native'
import MakeTheApiCall from '../apiCalls';
import { saveToLocalStorage, getStringFromLocalStorage, getFromLocalStorage } from '../Common'

/**
* login to app with username and password
* @param data
* @returns {axios.Promise}
*/

export const login = (data) => {
  let url = `${LOGIN_API}`
  return dispatch => {
    return MakeTheApiCall(url, 'POST', data).then(res => {
      console.log(res,121212)
      dispatch({
        type: GET_LOGIN_DATA,
        data: res.data
      })
      dispatch(saveToLocalStorage('token', res.data.token))
      if(res.data.user){
        let data = JSON.stringify(res.data.user)
        console.log(data,'data')
        dispatch(saveToLocalStorage('auth_data', data))
      }
      return res.data
    })
    .catch(function(error) {
      console.log(error,88888)
      dispatch({  
        type: HAS_ERROR,
        data: error.response.data,
      })
      return error.response.data
    })
  }
}

export const aahdaar = (params) => {
  let url = `${AAHDAAR_API}?aahdaar=`+params
  console.log(url,66)
  return dispatch => {
    return MakeTheApiCall(url, 'GET').then(res => {
      dispatch({
        type: GET_AAHDAAR_DATA,
        data: res.data
      })
      return res.data
    })
    .catch(function(error) {
      dispatch({
        type: HAS_ERROR,
        data: error,
      })
      return error
    })
  }
}

export const sendOtp = (params) => {
  let url = `${SENDOTP_API}?mode=`+params
  return dispatch => {
    return MakeTheApiCall(url, 'GET').then(res => {
      console.log(res,2222)
      dispatch({
        type: GET_OTP_DATA,
        data: res.data
      })
      return res.data
    })
    .catch(function(error) {
      console.log(error,333)
      dispatch({
        type: HAS_ERROR,
        data: error,
      })
      return error
    })
  }
}

export const validateOtp = (data) => {
  let url = `${VALIDATE_OTP_API}`
  return dispatch => {
    return MakeTheApiCall(url, 'POST', data).then(res => {
      dispatch({
        type: GET_VALIDATE_OTP_DATA,
        data: res.data
      })
      // if(res.data.user){
      // }
      return res.data
    })
    .catch(function(error) {
      dispatch({  
        type: HAS_ERROR,
        data: error.response.data,
      })
      return error.response.data
    })
  }
}

export const updateProps = () => {
  return async dispatch => {
    let token = await dispatch(getStringFromLocalStorage('token'))
    let userData = await dispatch(getFromLocalStorage('auth_data'))
    let userAuth = {token: token, user: userData}
    console.log(userAuth,4545)
    // console.log(data,'data')
    dispatch({
      type: GET_LOGIN_DATA,
      data: userAuth
    })
  }
}
