import axios from 'axios'
import { CREATE_ARTICLE_API } from '../apiConstant'
import { HAS_ERROR, CREATE_ARTICLE_DATA } from '../types'
import {AsyncStorage } from 'react-native'
import MakeTheApiCall from '../apiCalls';

export const createArticle = (data) => {
  let url = `${CREATE_ARTICLE_API}`
  console.log(url, "url")
  return async dispatch => {
    return axios.post(url, data,{
      headers: {
        'Authorization': 'jwt ' + await AsyncStorage.getItem("token")
      }
    }).then(res => {
      console.log(res, 1234)
      dispatch({
        type: CREATE_ARTICLE_DATA,
        data: res.data
      })
      return res.data
    })
    .catch(function(error) {
      console.log(error, 121211121)
      dispatch({  
        type: HAS_ERROR,
        data: error.response,
      })
      return error.response
    })
  }
}