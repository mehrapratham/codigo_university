import React from 'react';
import axios from 'axios';
// import { actions as toastrActions } from 'react-redux-toastr';
import {UPLOAD_PROGRESS} from './types'

//import {API_URL} from './ApiServerConfigs';
import store from '../store';
// import {logout, loggedIn} from '../services/authService.js'

let { dispatch, server } = store;
export default function MakeTheApiCall(url,method,data) {
    let options = GenerateOptions(url,method,data)

    return new Promise((resolve,reject) =>{
        return axios(options).then((response) =>{
            return resolve(response)
        }).catch((err) =>{
                if(err.status === 401){

                }
            return reject(err)
        });
    });
    
}
export function GenerateOptions(url = '', method = 'GET', data, contentType) {
    // var token = localStorage.getItem("frontend_token")
    var options = {
        method: method,
        url: `${url}`,
        crossDomain:true,
        // headers: {
        //     'Authorization': 'jwt ' + token
        // },
        json: true,
        onUploadProgress: function (progressEvent) {
          var percentCompleted = Math.round( (progressEvent.loaded * 100) / progressEvent.total );
          dispatch({
            type: UPLOAD_PROGRESS,
            data: percentCompleted,
          })
        }
    };
    if(contentType){
        options.headers['content-type'] = contentType;
    }
    if(!data) {
        return options;
    }

    if(method == 'GET') {
        options.params = data
    } else {
        options.data = data
    }

    return options;
}

