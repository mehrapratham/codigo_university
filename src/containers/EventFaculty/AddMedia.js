import React from 'react'
import { View, Text, Image, TouchableOpacity, ScrollView } from 'react-native'
import styles from '../../styles/styles'
import { Icon, Container, Content } from 'native-base'
import EventFormHeader from '../../components/Event/EventFormHeader';
import UploadButton from '../../components/Form/UploadButton';
import GradientButton from '../../components/Form/GradientButton';
import { Actions } from 'react-native-router-flux'

export default class AddMedia extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      image: [
        { img: require('../../img/multiImg.png') },
        { img: require('../../img/multiImg.png') },
        { img: require('../../img/multiImg.png') },
        { img: require('../../img/multiImg.png') },
        { img: require('../../img/multiImg.png') },
        { img: require('../../img/multiImg.png') },
        { img: require('../../img/multiImg.png') },
        { img: require('../../img/multiImg.png') },
        { img: require('../../img/multiImg.png') },
        { img: require('../../img/multiImg.png') },
        { img: require('../../img/multiImg.png') },
        { img: require('../../img/multiImg.png') },
        { img: require('../../img/multiImg.png') },
        { img: require('../../img/multiImg.png') },
        { img: require('../../img/multiImg.png') },
        { img: require('../../img/multiImg.png') },
        { img: require('../../img/multiImg.png') },
        { img: require('../../img/multiImg.png') },
        { img: require('../../img/multiImg.png') },
        { img: require('../../img/multiImg.png') },
        { img: require('../../img/multiImg.png') },
        { img: require('../../img/multiImg.png') },
      ],
      video: [
        { img: require('../../img/videothumb.jpeg') },
        { img: require('../../img/videothumb2.jpg') },
        { img: require('../../img/videothumb3.jpg') },
        { img: require('../../img/videothumb.jpeg') },
        { img: require('../../img/videothumb2.jpg') },
        { img: require('../../img/videothumb3.jpg') },
        { img: require('../../img/videothumb.jpeg') },
        { img: require('../../img/videothumb2.jpg') },
        { img: require('../../img/videothumb3.jpg') },
        { img: require('../../img/videothumb.jpeg') },
        { img: require('../../img/videothumb2.jpg') },
        { img: require('../../img/videothumb3.jpg') },
        { img: require('../../img/videothumb.jpeg') },
        { img: require('../../img/videothumb2.jpg') },
        { img: require('../../img/videothumb3.jpg') },
        { img: require('../../img/videothumb.jpeg') },
        { img: require('../../img/videothumb2.jpg') },
        { img: require('../../img/videothumb3.jpg') },
        { img: require('../../img/videothumb.jpeg') },
        { img: require('../../img/videothumb2.jpg') },
        { img: require('../../img/videothumb3.jpg') },
        { img: require('../../img/videothumb.jpeg') },
      ],
    }
  }
  render() {
    return (
      <Container style={styles.amMain}>
        <EventFormHeader step={2} />
        <Content style={styles.amCon}>
          <Text style={[styles.amTextMain, styles.marginBottom15]}>Add Media</Text>
          <View style={styles.marginBottom20}>
            <View style={styles.headingWithBorder}>
              <Text style={styles.headingWithBorderText}>Add Photos</Text>
              <Image source={require('../../img/arrowHeading.png')} style={styles.headingArrow} />
            </View>
            <View style={styles.formWorkArea}>
              <View style={[styles.amBtn1, styles.noMarginTop]}>
                <UploadButton btntext='Upload Picture' title='Add Photos' />
              </View>
              <View style={styles.amImgContainer}>
                {this.state.image.map((val, key) => {
                  return (
                    <View style={styles.amImgOut} key={key}>
                      <Image source={val.img} style={styles.stydBackImg} />
                    </View>
                  )
                })}
              </View>
            </View>
            <View style={styles.amEditTExt}>
              <TouchableOpacity style={styles.amEditTextOut}>
                <Image source={require('../../img/pencil.png')} />
                <Text style={styles.amEditTextMain}>Edit</Text>
              </TouchableOpacity>
            </View>
          </View>
          <View style={styles.marginBottom20}>
            <View style={styles.headingWithBorder}>
              <Text style={styles.headingWithBorderText}>Add Video</Text>
              <Image source={require('../../img/arrowHeading.png')} style={styles.headingArrow} />
            </View>
            <View style={styles.formWorkArea}>
              <View style={[styles.amBtn1, styles.noMarginTop]}>
                <UploadButton btntext='Upload Video' title='Add Video' />
              </View>
              <View style={styles.amImgContainer}>
                {this.state.video.map((val, key) => {
                  return (
                    <View style={styles.amImgOut} key={key}>
                      <Image source={val.img} style={styles.stydBackImg} />
                    </View>
                  )
                })}
              </View>
            </View>
          </View>

          <View style={styles.formButton}>
            <GradientButton onPress={()=> Actions.AddMembers()} label="NEXT" customHeight={35} />
          </View>
        </Content>
      </Container>
    )
  }
}