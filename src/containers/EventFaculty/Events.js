import React from 'react'
import { View, Text, Image, TouchableOpacity, ScrollView, TabHeading } from 'react-native'
import styles from '../../styles/styles'
import { Icon, Tabs, Tab, Accordion, ScrollableTab } from 'native-base'
import TextEditor from '../../components/Form/TextEditor'
import TextBox from '../../components/Form/TextBox'
import SelectBox from '../../components/Form/SelectBox'
import { Chip, Selectize } from 'react-native-material-selectize';
import ChipsBox from '../../components/Form/ChipsBox'
import {Video, MapView} from 'expo'
import VideoPlayer from '@expo/videoplayer';
import FeedActions from '../../components/common/FeedActions'
import AddComment from '../../components/common/AddComment'
import AddCommentTextarea from '../../components/common/AddCommentTextarea'
import WrittenBy from '../../components/common/WrittenBy'
import MemberImages from '../../components/common/MemberImages'
import { Actions } from 'react-native-router-flux'
import GradientButton from '../../components/Form/GradientButton'
import EventSkills from './EventSkills'
import FeedUser from '../../components/common/FeedUser'
import AccordionList from '../../components/common/CustomAccordion'
import CommentSection from '../../components/common/CommentSection'
import MultiImages from '../../components/common/MultiImages'

export default class Events extends React.Component {
	constructor(props) {
		super(props)
		this.state = {
			list:[
	      {
	        title: 'Why should you attend?',
	        body: 'React native Accordion/Collapse component, very good to use in toggles & show/hide content'
	      },
	      {
	        title: 'What is the event?',
	        body: 'AccordionList,Collapse,CollapseHeader & CollapseBody'
	      },
	      {
	        title: 'What is this event?',
	        body: 'AccordionList,Collapse,CollapseHeader & CollapseBody'
	      }
	     ],
		}
	}
	_head(item){
    return(
      <View style={styles.accordionEventHeader}>
        <Text style={styles.accordionHeaderText}>{item.title}</Text>
        
      </View>
    );
	}

	_body(item){
    return (
      <View style={styles.accordionEventBody}>
        <Text style={styles.accordionBodyText}>{item.body}</Text>
      </View>
    );
	}
	render(){
		return(
			<View style={styles.postCon}>
				<View style={[styles.postHeader, { backgroundColor: '#fbfbfb' }]}>
					<TouchableOpacity onPress={() => { Actions.pop() }}>
						<Icon name="arrow-back" style={[styles.closeBtn, styles.leftArrow]} />
					</TouchableOpacity>
					<View style={styles.nextToIcon}>
						<Text style={styles.topTitle}>Events</Text>
					</View>
					<View style={styles.rightSide}>

					</View>
				</View>
        <View style={[styles.workingArea, styles.darkGray]}>
        	<ScrollView>
        		<View style={[styles.mainbox,styles.hasWhiteBG,styles.marginTop15]}>
	        		<View style={[styles.webRelative,styles.marginBottom15]}>
	        			<Text style={styles.workshopText}>Workshop on Web Applications</Text>
	        			<View style={styles.webAbsolute}>
	        				<TouchableOpacity>
	        					<Image source={require('../../img/event_more.png')} style={styles.replyImg} />
	        				</TouchableOpacity>
	        			</View>
	      			</View>
	      			<View >
      					<Text><Text style={styles.eventCreateText}>Event Created on:</Text><Text style={styles.eventDateText}> 20th May 2018 at 2:20pm</Text></Text>
        			</View>
        		</View>
        		<View>
        			<Image source={require('../../img/event.png')} style={styles.eventBanner} />
        		</View>
	        	<View style={[styles.formCon, styles.hasWhiteBG, styles.marginBottom20]}>
	            <View style={styles.marginBottom20}>
	            	<Text style={[styles.eventCreateText,styles.marginBottom5]}>Date & Time</Text>
	            	<Text style={styles.eventDateText}>20th May 2018 at 2:20pm</Text>
	            </View>
	            <View style={styles.marginBottom20}>
	            	<Text style={[styles.eventCreateText,styles.marginBottom5]}>Venue</Text>
	            	<Text style={styles.eventDateText}>Dr. TMA Pai Hall, Manipal University</Text>
	            </View>
	            <Text style={styles.freeEventText}>Free Event</Text>
	        	</View>
	        	<View>
	        		<Tabs style={styles.tabStyle} locked={true} tabContainerStyle={{height: 45}} tabStyle={styles.tabStyle} tabBarUnderlineStyle={styles.tabBarUnderline} renderTabBar={()=> <ScrollableTab tabsContainerStyle={{backgroundColor: '#fff'}}/>}>
		            <Tab heading="ABOUT" tabStyle={styles.customTabStyle} activeTabStyle={styles.customTabStyle} activeTextStyle={styles.activeTabText} textStyle={styles.tabText} style={styles.atricleTabCon}>
			            <View>
			              <View style={[styles.tabContainer,styles.borderBottomWidth1]}>
			              	<View >
				            		<Text style={styles.aboutHeading}>About</Text>
				            		<Text style={styles.aboutEventContent}>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</Text>
				            		<TouchableOpacity>
				            			<Text style={styles.showMoreEvent}>Show more</Text>
				            		</TouchableOpacity>
			            		</View>
		            		</View>
		            		<View style={[styles.tabContainer,styles.borderBottomWidth1]}>
			            			<Text style={[styles.darkTextEvent,styles.marginBottom15]}>Skills you would bain by being a part of the Event</Text>
			            			<View style={[styles.eventTagsCon]}>
			            				<Text style={styles.eventTag}>HTML and CSS</Text>
			            				<Text style={styles.eventTag}>Javascript</Text>
			            				<Text style={styles.eventTag}>User Interface</Text>
			            				<Text style={styles.eventTag}>Python</Text>
			            				<Text style={styles.eventTag}>Libgdx</Text>
			            			</View>
			            	</View>
			            	<View style={[styles.tabContainer,styles.marginBottom15]}>
			            		<Text style={[styles.darkTextEvent,styles.marginBottom15]}>Speakers and Guests</Text>
			            		<View style={styles.relativeViewEvents}>
			            			<View style={styles.absoluteViewEvents}>
			            				<Image source={require('../../img/user.png')} style={styles.replyImg}/>
			            			</View>
			            			<View>
			            				<Text style={[styles.darkTextEvent,styles.marginBottom3]}>Prakash Rao</Text>
			            				<Text style={styles.eventCreateText}>Keynote Speaker</Text>
			            			</View>
			            		</View>
			            		<View style={styles.relativeViewEvents}>
			            			<View style={styles.absoluteViewEvents}>
			            				<Image source={require('../../img/user.png')} style={styles.replyImg}/>
			            			</View>
			            			<View>
			            				<Text style={[styles.darkTextEvent,styles.marginBottom3]}>Prakash Rao</Text>
			            				<Text style={styles.eventCreateText}>Keynote Speaker</Text>
			            			</View>
			            		</View>
			            	</View>
			            	<View style={styles.tabContainer}>
			            		<Text style={[styles.darkTextEvent,styles.marginBottom15]}>Event Location</Text>
			            		<View style={styles.relativeConEvent}>
			            			<View style={styles.absoluteConEvent}>
			            				<TouchableOpacity>
			            					<Image source={require('../../img/location.png')} style={styles.replyImg}/>
			            				</TouchableOpacity>
			            			</View>
			            			<Text style={styles.eventCreateText}>Dr. TMA Pai Hall, Manipal University</Text>
			            		</View>

			            	</View>
			            	<View style={styles.outerMapViewEvent}>
				            		<MapView
										        style={styles.mapViewEvent}
										        initialRegion={{
										          latitude: 37.78825,
										          longitude: -122.4324,
										          latitudeDelta: 0.0922,
										          longitudeDelta: 0.0421,
										        }}
										      />
							      </View>
							      <View style={[styles.marginBottom20,styles.tabContainer]}>
		            			<Text style={[styles.aboutHeading]}>Media</Text>
		            			<ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
		            				<View style={styles.mediaImgCon}>
			            				<View style={styles.mediaImg}>
			            					<Image source={require('../../img/media.png')} style={styles.ImgFullWidth} />
			            				</View>
			            				<View style={styles.mediaImg}>
			            					<Image source={require('../../img/media.png')} style={styles.ImgFullWidth} />
			            				</View>
			            				<View style={styles.mediaImg}>
			            					<Image source={require('../../img/media.png')} style={styles.ImgFullWidth} />
			            				</View>
		            				</View>
		            			</ScrollView>
		            		</View>
			            </View>
		            </Tab>
		            <Tab heading="SKILLS" tabStyle={styles.customTabStyle} activeTabStyle={styles.customTabStyle} activeTextStyle={styles.activeTabText} textStyle={styles.tabText} style={styles.atricleTabCon}>
									<EventSkills />
		            </Tab>
		            <Tab heading="DISCUSSIONS" tabStyle={styles.customTabStyle} activeTabStyle={styles.customTabStyle} activeTextStyle={styles.activeTabText} textStyle={styles.tabText} style={styles.atricleTabCon}>
		    				<View>
		            	<View style={styles.feedCon}>
										<View style={styles.feedHeader}>
											<View style={styles.marginBottom20}>
												<FeedUser />
											</View>
											<View>
												<Text style={styles.videoText}>This Video explain fluid statics and dynamics in detail.</Text>
											</View>
										</View>
										<View>
				        			<VideoPlayer
				               	videoProps={{
				                 	shouldPlay: false,
				                 	resizeMode: Video.RESIZE_MODE_CONTAIN,
				                 	source: {
				                   	uri: 'https://bitdash-a.akamaihd.net/content/sintel/hls/playlist.m3u8',
				                 	},
				               	}}
				                isPortrait={true}
				                playFromPositionMillis={0}
				              />
										</View>
										<View>
											<FeedActions />
										</View>
										<View>
											<AddComment />
										</View>
									</View>
									<View style={styles.feedCon}>
										<View style={styles.feedHeader}>
											<View style={styles.marginBottom20}>
												<FeedUser />
											</View>
											<View>
												<TouchableOpacity>
													<Text><Text style={styles.userfeedColor}>MRI-Safe Robots treat Epilepsy:</Text> <Text style={styles.userfeedLink}>http://www.google.com reminds me of this.</Text></Text>
												</TouchableOpacity>
											</View>
										</View>
										<View>
				        				<MultiImages />
										</View>
										<View>
											<FeedActions />
										</View>
										
										<View>
											<AddComment />
										</View>
									</View>
									<View style={styles.feedCon}>
										<View style={styles.feedHeader}>
											<View>
												<FeedUser userAction={true}/>
											</View>
										</View>
										<View style={[styles.feedHeader, styles.smallUserCon]}>
											<View style={styles.marginBottom20}>
												<FeedUser isSmall={true} size={36}/>
											</View>
											<View>
												<TouchableOpacity>
													<Text><Text style={styles.userfeedColor}>MRI-Safe Robots treat Epilepsy:</Text> <Text style={styles.userfeedLink}>http://www.google.com reminds me of this.</Text></Text>
												</TouchableOpacity>
											</View>
										</View>
										<View>
				        			<Image source={require('../../img/feed.png')} style={styles.feedImg} />
										</View>
										<View>
											<FeedActions />
										</View>
										<View style={styles.commentsOuterBox}>
											<CommentSection />
											<CommentSection />
											<CommentSection />
										</View>
										<View>
											<AddComment />
										</View>
									</View>
								</View>
		            </Tab>
		            <Tab heading="FAQS" tabStyle={styles.customTabStyle} activeTabStyle={styles.customTabStyle} activeTextStyle={styles.activeTabText} textStyle={styles.tabText} style={styles.atricleTabCon}>
		            	<View style={styles.tabContainer}>
		            		<Text style={styles.faqHeading}>Frequently Asked Questions</Text>
		            		<AccordionList
					            list={this.state.list}
					            header={this._head}
					            body={this._body}
					          />
					          <GradientButton label="Next" onPress={() => {Actions.RequestReview()}}/>
		            	</View>
		            </Tab>
		          </Tabs>
	        	</View>
        	</ScrollView>
        </View>
      </View>
		)
	}
}
