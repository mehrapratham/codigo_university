import React from 'react'
import { View, Text, Image, TouchableOpacity, ScrollView } from 'react-native'
import styles from '../../styles/styles'
import { Tabs, Tab, Icon } from 'native-base';
import GradientButton from '../../components/Form/GradientButton'

export default class RequestReview extends React.Component{
	constructor(props){
		super(props)
		this.state = {
			isVar: null,
			list: [
				{
					img: '',
					id: 2
				},
				{
					img: '',
					id: 3
				},
				{
					img: '',
					id: 4
				},
				{
					img: '',
					id: 5
				},
				{
					img: '',
					id: 6
				},
				{
					img: '',
					id: 7
				},
				{
					img: '',
					id: 8
				},
				{
					img: '',
					id: 9
				},
				{
					img: '',
					id: 10
				},
				{
					img: '',
					id: 11
				},
				{
					img: '',
					id: 12
				},
				{
					img: '',
					id: 13
				}
			]
		}
	}
	onOpen(key){
		if (this.state.isVar === key) {
			this.setState({isVar: null})
		} else {
			this.setState({isVar: key})
		}
		console.log(key)
	}
	onOpen1(){
		this.setState({isVar1: !this.state.isVar1})
	}
	onOpen2(){
		this.setState({isVar2: !this.state.isVar2})
	}
	render(){
		console.log(this.state.selectedId,666)
		return(
			<View style={styles.postCon}>
				<View style={[styles.postHeader, styles.hasBorderBottom]}>
					<TouchableOpacity onPress={() => { Actions.pop() }}>
						<Icon name="arrow-back" style={[styles.closeBtn, styles.leftArrow]} />
					</TouchableOpacity>
					<View style={styles.nextToIcon}>
						<Text style={styles.topTitle}>Dashboard</Text>
					</View>
					<View style={styles.rightSide}>
					</View>
				</View>
        <View style={styles.workingArea}>
        	<Tabs style={styles.tabStyle} tabContainerStyle={{height: 45}} tabStyle={styles.tabStyle} tabBarUnderlineStyle={styles.tabBarUnderline}>
            <Tab heading="REQUEST TO JOIN" tabStyle={styles.customTabStyle} activeTabStyle={styles.customTabStyle} activeTextStyle={styles.activeTabText} textStyle={styles.tabText} style={styles.atricleTabCon}>
              	<ScrollView showsVerticalScrollIndicator={false}>
              		<View style={styles.tabOuterBoxEventList}>
              			<View style={styles.reqstView}>
              				<View style={styles.relativeWireCon}>
              					<View>
              						<Text><Text style={styles.color40}>40</Text><Text style={styles.darkColor2}> Requests to be reviewed</Text></Text>
              					</View>
              					<View style={styles.wireImgView}>
              						<Image source={require('../../img/wire.png')} style={styles.replyImg}/>
              					</View>
              				</View>
              			</View>
              			{this.state.list.map((item,key) => {
              				return(
              					<TouchableOpacity key={key} style={[styles.mainbox,styles.borderBottomWidth2]} activeOpacity={0.7} onPress={this.onOpen.bind(this,key)}>
		              				<View style={styles.reviewRelativeCon}>
		              					<View style={styles.absoluteConWire}>
		              						<Image source={require('../../img/user.png')} style={styles.replyImg} />
		              					</View>
		              					<View>
		              						<Text style={[styles.marginBottom10,styles.mainName]}>Nettie Kelley</Text>
		              						<View style={[styles.relConList,(this.state.isVar === key) && styles.marginBottom10]}>
		              							<View>
		              								<Text><Text style={styles.lightColor}>Designation: </Text><Text style={styles.darkColor}>Faculty</Text></Text>
		              							</View>
		              							<View style={styles.absConList}>
		              								<Text><Text style={styles.lightColor}>Skill: </Text><Text style={styles.darkColor}>HTML & CSS</Text></Text>
		              							</View>
		              						</View>
		            							{(this.state.isVar === key) && <View>
			              						<Text style={styles.marginBottom10}><Text style={styles.lightColor}>Request sent on: </Text><Text style={styles.darkColor}>11-06-2018</Text></Text>
			              						<View style={styles.relConList}>
			              							<View>
			              								<GradientButton label="REJECT" smallGradientButtn={styles.smallGradientButtnReview} smallGradientText={styles.smallGradientTextReview} isFollow={true}/>
			              							</View>
			              							<View style={styles.absConList}>
			              								<GradientButton label="APPROVE" smallGradientButtn={styles.smallGradientButtnReview} smallGradientText={styles.smallGradientText}/>
			              							</View>
			              						</View>
		              						</View>}
		              					</View>
		              					<View style={styles.absoluteWireCon}>
			              					<TouchableOpacity>
			              						<Image source={(this.state.isVar != key) ? require('../../img/arrow_down.png'): require('../../img/arrow_up.png')} style={styles.replyImg}/>
			              					</TouchableOpacity>
		              					</View>
		              				</View>
		              			</TouchableOpacity>
              				)
              			})}
              			<View style={[styles.mainbox,styles.alignCenter]}>
              				<TouchableOpacity><Text style={styles.greenColor}>View More</Text></TouchableOpacity>
              			</View>
	              	</View>
              	</ScrollView>          
            </Tab>
            <Tab heading="STATUS OF REQUESTS" tabStyle={styles.customTabStyle} activeTabStyle={styles.customTabStyle} activeTextStyle={styles.activeTabText} textStyle={styles.tabText} style={styles.atricleTabCon}>
            	<ScrollView showsVerticalScrollIndicator={false}>
              		<View style={styles.tabOuterBoxEventList}>
              			
	              	</View>
              	</ScrollView> 
            </Tab>
          </Tabs>
        </View>
      </View>
		)
	}
}
