import React from 'react'
import { View, Text, Image, TouchableOpacity, ScrollView } from 'react-native'
import styles from '../../styles/styles'
import { Icon } from 'native-base'
import EventFormHeader from '../../components/Event/EventFormHeader'
import GradientButton from '../../components/Form/GradientButton'
import SelectBox from '../../components/Form/SelectBox'
import TextBox from '../../components/Form/TextBox'
import UploadButton from '../../components/Form/UploadButton';
import CheckboxGreen from '../../components/Form/CheckboxGreen'
import AccordionList from '../../components/common/CustomAccordion'
import { Actions } from 'react-native-router-flux'

export default class AddQuestions extends React.Component{
	constructor(props){
		super(props)
		this.state = {
			list:[
	      {
	        title: 'Why should you attend?',
	        body: 'React native Accordion/Collapse component, very good to use in toggles & show/hide content'
	      },
	      {
	        title: 'What is the event?',
	        body: 'AccordionList,Collapse,CollapseHeader & CollapseBody'
	      },
	      {
	        title: 'What is this event?',
	        body: 'AccordionList,Collapse,CollapseHeader & CollapseBody'
	      }
	     ],
		}
	}
	_head(item){
    return(
      <View style={[styles.accordionHeader, styles.hasWhiteBgAcc]}>
        <Text style={[styles.accordionHeaderText]}>{item.title}</Text>
        
      </View>
    );
	}

	_body(item){
    return (
      <View style={[styles.accordionBody, styles.hasWhiteBgAccBody]}>
        <Text style={[styles.accordionBodyText, styles.accordionWhiteBodyText]}>{item.body}</Text>
        <View>

        </View>
      </View>
    );
	}

	render(){
		return(
			<View style={styles.eventFormCon}>
				<EventFormHeader step={4} />
				<View style={styles.eventFormWorkArea}>
					<ScrollView>
						<Text style={[styles.amTextMain, styles.marginBottom15]}>Frequently Asked Questions</Text>
						<View style={[styles.eventQuestionsCon, styles.marginBottom20]}>
							<View style={styles.marginBottom10}>
								<AccordionList
			            list={this.state.list}
			            header={this._head}
			            body={this._body}
			            whiteBg={true}
			          />
							</View>
							<UploadButton btntext="+Add Questions" />
						</View>

						<View style={styles.formButton}>
							<GradientButton onPress={()=> Actions.Events()} label="CREATE EVENT" customHeight={35} />
						</View>
					</ScrollView>
				</View>
      </View>
		)
	}
}
