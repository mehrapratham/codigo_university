import React from 'react'
import { View, Text, Image, TouchableOpacity, ScrollView } from 'react-native'
import styles from '../../styles/styles'
import { Icon } from 'native-base'
import EventFormHeader from '../../components/Event/EventFormHeader'
import GradientButton from '../../components/Form/GradientButton'
import SelectBox from '../../components/Form/SelectBox'
import TextBox from '../../components/Form/TextBox'
import UploadButton from '../../components/Form/UploadButton';
import CheckboxGreen from '../../components/Form/CheckboxGreen'
import { Actions } from 'react-native-router-flux'

export default class AddMembers extends React.Component{
	constructor(props){
		super(props)
		this.state = {
		}
	}
	render(){
		return(
			<View style={styles.eventFormCon}>
				<EventFormHeader step={3} />
				<View style={styles.eventFormWorkArea}>
					<ScrollView>
						<Text style={[styles.amTextMain, styles.marginBottom15]}>Invite Members</Text>
						<View>
							<View style={styles.headingWithBorder}>
								<Text style={styles.headingWithBorderText}>Invite Members to co-host</Text>
								<Image source={require('../../img/arrowHeading.png')} style={styles.headingArrow} />
							</View>
							<View style={[styles.formWorkArea, styles.marginBottom20]}>
								<TextBox label="Invite Members you wish to co-host with you" labelColor="#6685a3" hasWhitebg={true} placeholder="Add colleges or Members" height={40} marginBottom={20} />
								<TextBox label="Send a message to each Member to each Member you wish to co-host with you" labelColor="#6685a3" hasWhitebg={true} placeholder="Enter Message..." height={100} multiline={true} marginBottom={20} />
								<View>
									<Text style={styles.checkboxheading}>Co-host can</Text>
									<View style={styles.greenCheckboxCon}>
										<CheckboxGreen label="Modify the Event" />
										<CheckboxGreen label="Invite more members" />
										<CheckboxGreen label="See Attendees list" textColor={'#104cb0'} />
									</View>
								</View>
								
							</View>
						</View>
						<View>
							<View style={styles.headingWithBorder}>
								<Text style={styles.headingWithBorderText}>Invite Members to attend the event</Text>
								<Image source={require('../../img/arrowHeading.png')} style={styles.headingArrow} />
							</View>
							<View style={[styles.formWorkArea, styles.marginBottom20]}>
								<TextBox label="Add Colleges and Members you wish to invite to the Event" labelColor="#6685a3" hasWhitebg={true} placeholder="Add colleges or Members" height={40} marginBottom={20} />
								<TextBox label="Send a message" labelColor="#6685a3" hasWhitebg={true} placeholder="Enter Message..." height={100} multiline={true} marginBottom={20} />
								
								
							</View>
						</View>

						<View>
							<View style={styles.headingWithBorder}>
								<Text style={styles.headingWithBorderText}>Invite Members to attend the event</Text>
							</View>
							<View style={[styles.formWorkArea, styles.marginBottom20]}>
								<View>
									<Text style={[styles.checkboxheading, styles.fontSize12]}>Attendees can</Text>
									<View style={styles.greenCheckboxCon}>
										<CheckboxGreen label="Anyone can post content to the event" />
										<CheckboxGreen label="Only host and co-hosts can post content to the event" />
										<CheckboxGreen label="Content posted by the attendees needs to be validated by the host" textColor={'#104cb0'} />
									</View>
								</View>
							</View>
						</View>

						<View style={styles.formButton}>
							<GradientButton onPress={()=> Actions.AddQuestions()} label="NEXT" customHeight={35} />
						</View>
					</ScrollView>
				</View>
      </View>
		)
	}
}
