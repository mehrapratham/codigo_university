import React from 'react'
import { View, Text, TouchableOpacity, Dimensions } from 'react-native'
import TopHeader from '../../components/common/header'
import TextBox from '../../components/Form/TextBox'
import { Container, Content, Button } from 'native-base';
import GradientButton from '../../components/Form/GradientButton'
import styles from '../../styles/styles'
import ProgressBar from '../../components/common/ProgressBar'
import {RadioGroup, RadioButton} from 'react-native-flexi-radio-button'
import RadioBtn from '../../components/Form/RadioBtn'
import { Actions } from 'react-native-router-flux'
import { connect } from 'react-redux'
import { sendOtp } from '../../actions/Auth'
import Toast from 'react-native-easy-toast'

class ReceiveOtp extends React.Component{
	constructor(props){
		super(props);
		this.state = {
			recieveOtp: '',
			loader: false
		}
	}
	onchange(key, value){
		let {recieveOtp} = this.state;
		this.setState({recieveOtp: value})
	}
	onSubmit(){
		const { recieveOtp } = this.state;
		this.setState({ loader: true })
		if(recieveOtp){
			this.props.dispatch(sendOtp(recieveOtp)).then(res => {
				this.setState({ loader: false })
				if(res.message){
					this.setState({ loader: false })
					Actions.securityCode()
				}
			})
		}else {
			this.setState({ loader: false })
			this.refs.toast.show('Some field are missing')
		}
	}
	render(){
		let userData = this.props.GetStarted.aadharData
		var {height, width} = Dimensions.get('window')
		console.log(this.props,"sdsdsds")
		return(
			<Container style={styles.topBox}>
				<TopHeader title="Receive OTP" />
				<Content>
					<ProgressBar totalStep={8} currentStep={2} keyboardType='numeric' />
					<View style={styles.mainbox}>
						<View>
							<Text style={[styles.boxlabel, styles.marginBottom10]}>How do you want to validate your account?</Text>
						</View>
						<View style={styles.marginBottom40}>
							<RadioGroup highlightColor="#f0f3f6" size={16} color="#9b9b9b" onSelect = {(index, value) => this.onchange(index, value)}>
				        <RadioButton style={[styles.radioBox,styles.hasGrayBackground]} value={userData.email} >
				          <Text style={styles.radioBtnText}>{userData.email}</Text>
				        </RadioButton>
				        <RadioButton style={[styles.radioBox,styles.hasGrayBackground]} value={userData.mobile}>
				          <Text style={styles.radioBtnText}>{userData.mobile}</Text>
				        </RadioButton>
					    </RadioGroup>
						</View>
            <View>
	          	<GradientButton label={this.state.loader ? "LOADER": "SEND OTP"} onPress={this.onSubmit.bind(this)} disabled={this.state.loader ? true: false} pointerEvents={this.state.loader ? 'none': ''}/>
	       		</View>
    			</View>
    			<Toast
						ref="toast"
						style={{ backgroundColor: 'rgba(255,0,0,0.4)', width: width - 40 }}
						position='bottom'
						fadeInDuration={1000}
						fadeOutDuration={1000}
						opacity={0.8}
						textStyle={{ color: '#fff',textAlign: 'center' }}
					/>
				</Content>
			</Container>
		)
	}
}
export default connect(state => ({
}, mapDispatch))(ReceiveOtp);

const mapDispatch = (dispatch) => {
   const allActionProps = Object.assign({}, dispatch);
   return allActionProps;
}
