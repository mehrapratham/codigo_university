import React from 'react'
import { View, Text, TouchableOpacity } from 'react-native'
import TopHeader from '../../components/common/header'
import TextBox from '../../components/Form/TextBox'
import { Container, Content, Button } from 'native-base';
import GradientButton from '../../components/Form/GradientButton'
import styles from '../../styles/styles'
import ProgressBar from '../../components/common/ProgressBar'
import {RadioGroup, RadioButton} from 'react-native-flexi-radio-button'
import RadioBtn from '../../components/Form/RadioBtn'
import OtpInputs from 'react-native-otp-inputs'
import { Actions } from 'react-native-router-flux'
import { connect } from 'react-redux'
import { validateOtp } from '../../actions/Auth'
import Toast from 'react-native-easy-toast'

class securityCode extends React.Component{
	constructor(){
		super();
		this.state = {
			otpCode: '2345674',
			loader: false
		}
	}
	onchange(code){
		// this.setState({otpCode: code})
	}
	onSubmit(){
		const {otpCode} = this.state;
		this.setState({ loader: true})
		this.props.dispatch(validateOtp(otpCode)).then(res => {
			this.setState({ loader: false })
			console.log(res)
			if(res === 'success'){
				Actions.SelectProfile()
			}
		})
	}
	render(){
		let otpData = this.props.GetStarted.otpData
		console.log(otpData,66)
		return(
			<Container style={styles.topBox}>
				<TopHeader title="Security Code" />
				<ProgressBar totalStep={8} currentStep={3} keyboardType='numeric' />
				<Content>
					<View style={[styles.mainbox,styles.paddingLeft10]}>
						<View>
							<Text style={[styles.boxlabel,styles.marginBottomZero]}>Enter the Security code to your registered</Text>
							<Text style={[styles.fontSize12,styles.darkBlueColor]}>Email ID</Text>
						</View>
					</View>
					<View style={styles.marginBottom30}>
						<OtpInputs handleChange={(code) => {this.onchange(code)}} inputStyles={styles.otpColor} numberOfInputs={4} inputContainerStyles={[styles.otpInputColor]} focusedBorderColor="#55bf8f" keyboardType="numeric-pad" />
						<View style={styles.centered}><TouchableOpacity><Text style={styles.resendCodeText}>Resend Code</Text></TouchableOpacity></View>
					</View>
					<View style={styles.mainbox}>	
            <View>
	          	<GradientButton label={this.state.loader ? "LOADER":"NEXT"} disabled={this.state.loader ? true: false} pointerEvents={this.state.loader ? 'none': ''} onPress={this.onSubmit.bind(this)} />
		        </View>
    			</View>
				</Content>
			</Container>
		)
	}
}
export default connect(state => ({
}, mapDispatch))(securityCode);

const mapDispatch = (dispatch) => {
   const allActionProps = Object.assign({}, dispatch);
   return allActionProps;
}
