import React from 'react'
import { View, Text, TouchableOpacity, Dimensions, Image } from 'react-native'
import TopHeader from '../../components/common/header'
import TextBox from '../../components/Form/TextBox'
import SelectBox from '../../components/Form/SelectBox'
import { Container, Content, Button } from 'native-base';
import GradientButton from '../../components/Form/GradientButton'
import styles from '../../styles/styles'
import ProgressBar from '../../components/common/ProgressBar'
import { Actions } from 'react-native-router-flux'
import { connect } from 'react-redux'
import { aahdaar } from '../../actions/Auth'
import Toast from 'react-native-easy-toast'

class UploadIdDetail extends React.Component{
	constructor(props){
		super(props);
		this.state = {
			
		}
	}
	render(){
		var {height, width} = Dimensions.get('window')
		return(
			<Container style={styles.topBox}>
				<TopHeader title="Upload Valid ID" />
				<Content>
					<ProgressBar totalStep={8} currentStep={1} keyboardType='numeric' />
					<View style={[styles.mainbox]}>
						<View style={styles.marginBottom30}>
							<Text style={styles.boxlabel}>Passport Number</Text>
							<Text style={styles.infoText}>J3245879</Text>
						</View>
						<View style={styles.marginBottom30}>
							<Text style={styles.boxlabel}>Surname</Text>
							<Text style={styles.infoText}>Rupinderpal Singh</Text>
						</View>
						<View style={styles.marginBottom30}>
							<Text style={styles.boxlabel}>Given Name (s)</Text>
							<Text style={styles.infoText}>Rupi</Text>
						</View>
						<View style={styles.marginBottom30}>
							<Text style={styles.boxlabel}>Date of Birth</Text>
							<Text style={styles.infoText}>25/06/1992</Text>
						</View>
						<View style={styles.marginBottom30}>
							<Text style={styles.boxlabel}>Address</Text>
							<Text style={styles.infoText}>P.O. Box 38822, P.C-112 {"\n"}Abu Dhabi, UAE</Text>
						</View>

            <TextBox label="Enter your Mobile Number" />
            <TextBox label="Enter your Email ID" />
            <View>
	          	<GradientButton label={"UPLOAD PICTURE"} />
            </View>
	        </View>
				</Content>
			</Container>
		)
	}
}
export default connect(state => ({
}, mapDispatch))(UploadIdDetail);


const mapDispatch = (dispatch) => {
   const allActionProps = Object.assign({}, dispatch);
   return allActionProps;
}
