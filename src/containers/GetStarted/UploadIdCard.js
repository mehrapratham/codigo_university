import React from 'react'
import { View, Text, TouchableOpacity, Dimensions, Image } from 'react-native'
import TopHeader from '../../components/common/header'
import TextBox from '../../components/Form/TextBox'
import SelectBox from '../../components/Form/SelectBox'
import { Container, Content, Button } from 'native-base';
import GradientButton from '../../components/Form/GradientButton'
import styles from '../../styles/styles'
import ProgressBar from '../../components/common/ProgressBar'
import { Actions } from 'react-native-router-flux'
import { connect } from 'react-redux'
import { aahdaar } from '../../actions/Auth'
import Toast from 'react-native-easy-toast'

class UploadIdCard extends React.Component{
	constructor(props){
		super(props);
		this.state = {
			aadhaarData: {
				aahdaar: '',
				loader: false
			}
		}
	}
	onchange(key, value){
		let {aadhaarData} = this.state;
		aadhaarData[key] = value;
		this.setState({aadhaarData})
	}
	onSubmit(){
		const { aadhaarData } = this.state;
		this.setState({ loader: true })
		if(aadhaarData.aahdaar){
			this.props.dispatch(aahdaar(aadhaarData.aahdaar)).then(res => {
				this.setState({ loader: false })
				if(res.adhaar_data){
					this.setState({ loader: false })
					Actions.ReceiveOtp()
				}
			})
		}else {
			this.setState({ loader: false })
			this.refs.toast.show('Aahdaar field are missing')
		}
	}
	render(){
		var {height, width} = Dimensions.get('window')
		return(
			<Container style={styles.topBox}>
				<TopHeader title="Upload Valid ID" />
				<Content>
					<ProgressBar totalStep={8} currentStep={1} keyboardType='numeric' />
					<View style={[styles.mainbox]}>
	            <SelectBox label="Select ID Type" marginBottom={40} width={110} />
	            <View style={styles.marginBottom20}>
	            	<View style={styles.flexDirectionRow}>
	            		<Text style={styles.normalText}>Upload an image of valid ID</Text>
		          	</View>
		          	<GradientButton disabled={true} label={"UPLOAD PICTURE"} />
	            </View>
	            <View style={styles.errorCon}>
	            	<Image source={require('../../img/error.png')} style={styles.errorIcon} />
	            	<Text style={styles.errorText}>Select ID type before uploading image</Text>
	            </View>
	        </View>
				</Content>
			</Container>
		)
	}
}
export default connect(state => ({
}, mapDispatch))(UploadIdCard);


const mapDispatch = (dispatch) => {
   const allActionProps = Object.assign({}, dispatch);
   return allActionProps;
}
