import React from 'react'
import { View, Text, TouchableOpacity, Dimensions, Image } from 'react-native'
import TopHeader from '../../components/common/header'
import TextBox from '../../components/Form/TextBox'
import SelectBox from '../../components/Form/SelectBox'
import { Container, Content, Button } from 'native-base';
import GradientButton from '../../components/Form/GradientButton'
import styles from '../../styles/styles'
import ProgressBar from '../../components/common/ProgressBar'
import { Actions } from 'react-native-router-flux'
import { connect } from 'react-redux'
import { aahdaar } from '../../actions/Auth'
import Toast from 'react-native-easy-toast'

class ThankYouScreen extends React.Component{
	constructor(props){
		super(props);
		this.state = {
			
		}
	}
	
	render(){
		var {height, width} = Dimensions.get('window')
		return(
			<Container style={styles.topBox}>
				<Content contentContainerStyle={styles.hasFlex}>
					<View style={[styles.mainbox, styles.hasFlex, {paddingBottom: 50}]}>
            <View style={[styles.hasFlex, styles.thankYouCon]}>
            	<Image source={require('../../img/successIcon.png')} style={styles.thankYouIcon} />
            	<Text style={styles.thankYouHeading}>Thank you for your request</Text>
            	<Text style={styles.thankYouSubHeading}>Our team will be in touch with you shortly. Meanwhile check out our website to know us better.</Text>
            </View>
            <View style={styles.thankuButton}>
	          	<GradientButton label={"GO TO UNIVERSITY.SOCIAL"} />
            </View>
	        </View>
				</Content>
			</Container>
		)
	}
}
export default connect(state => ({
}, mapDispatch))(ThankYouScreen);


const mapDispatch = (dispatch) => {
   const allActionProps = Object.assign({}, dispatch);
   return allActionProps;
}
