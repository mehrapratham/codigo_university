import React from 'react'
import { View, Text, TouchableOpacity } from 'react-native'
import TopHeader from '../../components/common/header'
import TextBox from '../../components/Form/TextBox'
import { Container, Content, Button } from 'native-base';
import GradientButton from '../../components/Form/GradientButton'
import styles from '../../styles/styles'
import ProgressBar from '../../components/common/ProgressBar'
import {RadioGroup, RadioButton} from 'react-native-flexi-radio-button'
import RadioBtn from '../../components/Form/RadioBtn'
import { Actions } from 'react-native-router-flux'

export default class resetPassword extends React.Component{
	render(){
		return(
			<Container style={styles.topBox}>
				<TopHeader title="Reset Password" />
				<Content style={styles.hasGrayBackground}>
					<ProgressBar totalStep={8} currentStep={5} keyboardType='numeric' />
					<View style={styles.mainbox}>
						<View>
							<Text style={[styles.bigHeading]}>Hi Nikhil!</Text>
						</View>
						<View>
							<TextBox label="Enter new password" hasWhitebg={true}/>
		          			<TextBox label="Confirm Password" hasWhitebg={true} secureTextEntry={true} />
						</View>
						<View style={styles.marginBottom30}>
							<Text style={styles.btmHeading}>Your password needs to include</Text>
							<View style={styles.upperDotViewStyle}><Text style={styles.textWithBullet}>at least one number or symbol.</Text><Text style={styles.dotStyle}></Text></View>
							<View style={styles.upperDotViewStyle}><Text style={styles.textWithBullet}>both lower and upper case latin characters.</Text><Text style={styles.dotStyle}></Text></View>
							<View style={styles.upperDotViewStyle}><Text style={styles.textWithBullet}>have atleast minimum of 8 characters.</Text><Text style={styles.dotStyle}></Text></View>
						</View>
				        <View>
				          	<GradientButton label="DONE" onPress={() => {Actions.SelectSkills()}}/>
				        </View>
          			</View>
				</Content>
			</Container>
		)
	}
}
