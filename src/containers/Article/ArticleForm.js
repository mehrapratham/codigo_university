import React from 'react'
import { View, Text, Image, TouchableOpacity, ScrollView } from 'react-native'
import styles from '../../styles/styles'
import {Icon} from 'native-base'
import TextEditor from '../../components/Form/TextEditor'
import TextBox from '../../components/Form/TextBox'
import SelectBox from '../../components/Form/SelectBox'
import { Chip, Selectize } from 'react-native-material-selectize';
import ChipsBox from '../../components/Form/ChipsBox'
import { Actions } from 'react-native-router-flux'
import { updateProps } from '../../actions/Auth'
import { connect } from 'react-redux'

class ArticleForm extends React.Component{
	constructor(props){
		super(props)
		this.state = {
			currentTags: ['Writing', 'Marketing'],
			participants: ['Rupinderpal', 'Pratham'],
			privacy: [
				{
					label: 'Private',
					value: 'PR'
				},
				{
					label: 'Public',
					value: 'PU'
				},
			],
			selectedValue: ''
		}
	}
	onValueChange(key,value) {
		console.log(key,value,33)
			this.setState({ selectedValue: value })
  }
	render(){
		console.log(this.state.selectedValue,'state')
		return(
			<View style={styles.postCon}>
				<View style={styles.postHeader}>
					<TouchableOpacity onPress={() => {Actions.pop()}}>
						<Icon name="close" style={styles.closeBtn} />
					</TouchableOpacity>
					<View style={styles.rightSide}>
						<TouchableOpacity style={styles.headerLinkBtn} onPress={() => {Actions.ArticleDetail()}}>
							<Text style={styles.headerLink}>Next</Text>
						</TouchableOpacity>
					</View>
				</View>
		        <View style={styles.workingArea}>
		        	<ScrollView>
			        	<View style={[styles.formCon, styles.bringToTop]}>
			        		<Text style={styles.formHeading}>Create an article</Text>
			        		<SelectBox label="Select Program" marginBottom={15} hasWhitebg={true} height={45} placeholder="Select Program"/>
			        		
			        		<SelectBox label="Select Privacy" marginBottom={15} hasWhitebg={true} data={this.state.privacy} leftText="This article is viewable by all" height={45} selectedValue={this.state.selectedValue} placeholder="Select Privacy" onValueChange={this.onValueChange.bind(this, 'selectedValue')} valueProp={'value'} labelProp={'label'}/>
		        					
			        		<ChipsBox label="Add tags" inputPlaceholder="Search Tag" chips={this.state.currentTags} />
			        		<Text style={[styles.formHeading, styles.subHeading]}>Add Skills</Text>
			        	</View>
			        	<View style={[styles.formCon, styles.addSkills]}>
			        		<TextBox label="Add a skill associated with the article" marginBottom={15} hasWhitebg={true} />
			        		<ChipsBox label="Add a participant to the skill" inputPlaceholder="Search Participant" chips={this.state.participants} />
			        		<View style={styles.addSkillBtnCon}>
			        			<TouchableOpacity>
									<Text style={styles.cancelBtnText}>Cancel</Text>
									</TouchableOpacity>
									<View style={styles.lineDivider}></View>
									<TouchableOpacity>
										<Text style={styles.LinkColor}>Done</Text>
									</TouchableOpacity>
			        		</View>
			        	</View>
			        	<View>
			        		<TouchableOpacity style={styles.bigLengthBtn}>
			        			<Text style={[styles.LinkColor, styles.boldText,styles.greenColor,styles.addParticipantText]}>+ Add Skill</Text>
			        		</TouchableOpacity>
			        	</View>
		        	</ScrollView>
	        </View>
      </View>
		)
	}
}
export default connect(state => ({
}, mapDispatch))(ArticleForm);


const mapDispatch = (dispatch) => {
   const allActionProps = Object.assign({}, dispatch);
   return allActionProps;
}
