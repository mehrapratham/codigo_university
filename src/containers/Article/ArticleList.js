import React from 'react'
import { View, Text, Image, TouchableOpacity, ScrollView } from 'react-native'
import styles from '../../styles/styles'
import {Icon} from 'native-base'
import { Tabs, Tab } from 'native-base';

export default class ArticleList extends React.Component{
	constructor(props){
		super(props)
		this.state = {
			currentTags: ['Writing', 'Marketing'],
			participants: ['Rupinderpal', 'Pratham']
		}
	}
	render(){
		return(
			<View style={styles.postCon}>
				<View style={[styles.postHeader, styles.hasBorderBottom]}>
					<TouchableOpacity>
						<Icon name="arrow-back" style={[styles.closeBtn, styles.leftArrow]} />
					</TouchableOpacity>
					<View style={styles.nextToIcon}>
						<Text>Articles</Text>
					</View>
					<View style={styles.rightSide}>
					</View>
				</View>
        <View style={styles.workingArea}>
        	<Tabs style={styles.tabStyle} tabContainerStyle={{height: 40}} tabStyle={styles.tabStyle} tabBarUnderlineStyle={styles.tabBarUnderline}>
            <Tab heading="ONGOING" tabStyle={styles.customTabStyle} activeTabStyle={styles.customTabStyle} activeTextStyle={styles.activeTabText} textStyle={styles.tabText} style={styles.atricleTabCon}>
              <View style={styles.tabOuterBox}>
              	<ScrollView showsVerticalScrollIndicator={false}>
	              	<TouchableOpacity style={styles.atricleList} activeOpacity={0.7}>
	              		<Text style={styles.articleTitle}>Create right marketing mix</Text>
	              		<Text style={styles.atricleSubheading}>Edited 2 hour ago</Text>
	              		<Image source={require('../../img/threedot.png')} style={styles.atricleActionIcon}/>
	              	</TouchableOpacity>
	              	<TouchableOpacity style={styles.atricleList} activeOpacity={0.7}>
	              		<Text style={styles.articleTitle}>Create right marketing mix</Text>
	              		<Text style={styles.atricleSubheading}>Edited 2 hour ago</Text>
	              		<Image source={require('../../img/threedot.png')} style={styles.atricleActionIcon}/>
	              	</TouchableOpacity>
	              	<TouchableOpacity style={styles.atricleList} activeOpacity={0.7}>
	              		<Text style={styles.articleTitle}>Create right marketing mix</Text>
	              		<Text style={styles.atricleSubheading}>Edited 2 hour ago</Text>
	              		<Image source={require('../../img/threedot.png')} style={styles.atricleActionIcon}/>
	              	</TouchableOpacity>
	              	<TouchableOpacity style={styles.atricleList} activeOpacity={0.7}>
	              		<Text style={styles.articleTitle}>Create right marketing mix</Text>
	              		<Text style={styles.atricleSubheading}>Edited 2 hour ago</Text>
	              		<Image source={require('../../img/threedot.png')} style={styles.atricleActionIcon}/>
	              	</TouchableOpacity>
	              	<TouchableOpacity style={styles.atricleList} activeOpacity={0.7}>
	              		<Text style={styles.articleTitle}>Create right marketing mix</Text>
	              		<Text style={styles.atricleSubheading}>Edited 2 hour ago</Text>
	              		<Image source={require('../../img/threedot.png')} style={styles.atricleActionIcon}/>
	              	</TouchableOpacity>
	              	<TouchableOpacity style={styles.atricleList} activeOpacity={0.7}>
	              		<Text style={styles.articleTitle}>Create right marketing mix</Text>
	              		<Text style={styles.atricleSubheading}>Edited 2 hour ago</Text>
	              		<Image source={require('../../img/threedot.png')} style={styles.atricleActionIcon}/>
	              	</TouchableOpacity>
	              	<TouchableOpacity style={styles.atricleList} activeOpacity={0.7}>
	              		<Text style={styles.articleTitle}>Create right marketing mix</Text>
	              		<Text style={styles.atricleSubheading}>Edited 2 hour ago</Text>
	              		<Image source={require('../../img/threedot.png')} style={styles.atricleActionIcon}/>
	              	</TouchableOpacity>
              	</ScrollView>
              </View>
            </Tab>
            <Tab heading="PUBLISHED" tabStyle={styles.customTabStyle} activeTabStyle={styles.customTabStyle} activeTextStyle={styles.activeTabText} textStyle={styles.tabText} style={styles.atricleTabCon}>
            	<View style={styles.tabOuterBox}>
	              <ScrollView showsVerticalScrollIndicator={false}>
	              	<TouchableOpacity style={styles.atricleList} activeOpacity={0.7}>
	              		<Text style={styles.articleTitle}>Create right marketing mix</Text>
	              		<Text style={styles.atricleSubheading}>Edited 2 hour ago</Text>
	              		<Image source={require('../../img/threedot.png')} style={styles.atricleActionIcon}/>
	              	</TouchableOpacity>
	              	<TouchableOpacity style={styles.atricleList} activeOpacity={0.7}>
	              		<Text style={styles.articleTitle}>Create right marketing mix</Text>
	              		<Text style={styles.atricleSubheading}>Edited 2 hour ago</Text>
	              		<Image source={require('../../img/threedot.png')} style={styles.atricleActionIcon}/>
	              	</TouchableOpacity>
	              	<TouchableOpacity style={styles.atricleList} activeOpacity={0.7}>
	              		<Text style={styles.articleTitle}>Create right marketing mix</Text>
	              		<Text style={styles.atricleSubheading}>Edited 2 hour ago</Text>
	              		<Image source={require('../../img/threedot.png')} style={styles.atricleActionIcon}/>
	              	</TouchableOpacity>
	              	<TouchableOpacity style={styles.atricleList} activeOpacity={0.7}>
	              		<Text style={styles.articleTitle}>Create right marketing mix</Text>
	              		<Text style={styles.atricleSubheading}>Edited 2 hour ago</Text>
	              		<Image source={require('../../img/threedot.png')} style={styles.atricleActionIcon}/>
	              	</TouchableOpacity>
	              	<TouchableOpacity style={styles.atricleList} activeOpacity={0.7}>
	              		<Text style={styles.articleTitle}>Create right marketing mix</Text>
	              		<Text style={styles.atricleSubheading}>Edited 2 hour ago</Text>
	              		<Image source={require('../../img/threedot.png')} style={styles.atricleActionIcon}/>
	              	</TouchableOpacity>
	              	<TouchableOpacity style={styles.atricleList} activeOpacity={0.7}>
	              		<Text style={styles.articleTitle}>Create right marketing mix</Text>
	              		<Text style={styles.atricleSubheading}>Edited 2 hour ago</Text>
	              		<Image source={require('../../img/threedot.png')} style={styles.atricleActionIcon}/>
	              	</TouchableOpacity>
	              	<TouchableOpacity style={styles.atricleList} activeOpacity={0.7}>
	              		<Text style={styles.articleTitle}>Create right marketing mix</Text>
	              		<Text style={styles.atricleSubheading}>Edited 2 hour ago</Text>
	              		<Image source={require('../../img/threedot.png')} style={styles.atricleActionIcon}/>
	              	</TouchableOpacity>
	              </ScrollView>
              </View>
            </Tab>
          </Tabs>
        </View>
      </View>
		)
	}
}
