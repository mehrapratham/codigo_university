import React from 'react'
import { View, Text, Image, TouchableOpacity } from 'react-native'
import styles from '../../styles/styles'
import {Icon} from 'native-base'
import TextEditor from '../../components/Form/TextEditor'
import { Actions } from 'react-native-router-flux'
import TextBox from '../../components/Form/TextBox'
import { createArticle } from '../../actions/Article'
import { updateProps } from '../../actions/Auth'
import { connect } from 'react-redux'

import {WebViewQuillEditor, WebViewQuillViewer} from '../../components/react-native-webview-quilljs'
import { QuillDeltaToHtmlConverter } from 'quill-delta-to-html';

const contentToDisplay = "<p>test</p>"

class ArticleFirstStep extends React.Component{
	constructor(props){
		super(props);
		this.state = {
			loader: false
		}
	}
	componentWillMount(){
		this.props.dispatch(updateProps())
	}

	submitContent(content){
		this.setState({loader: true})
		let id = this.props.Auth.authList.user.id
		let create_by = this.props.Auth.authList.id
		let articleData = {title : 'test title', content: content, created_by: id}
		console.log(articleData)
		if (articleData.title == '' || articleData.content == '') {
			console.log(1211211)
			console.log('pending')
		} else {
			this.props.dispatch(createArticle(articleData)).then(res=>{
				this.setState({loader: false})
				console.log(res,2323232)
				if(res.id){
					Actions.ArticleForm()
				}
			})
		}
	}

	getDeltaCallback = (delta) => {
      var cfg = {};
      var converter = new QuillDeltaToHtmlConverter(delta.delta.ops, cfg);
      var html = converter.convert(); 
      this.submitContent(html)
  }

	getContent(){
		alert('here')
		let data = this.webViewQuillEditor.getDelta();
	}

	onLoadCallback(){
		// console.log('here')
	}

	render(){
		console.log(this.props,66)
		return(
			<View style={styles.postCon}>
				<View style={styles.postHeader}>
					<TouchableOpacity onPress={() => {Actions.pop()}}>
						<Icon name="close" style={styles.closeBtn} />
					</TouchableOpacity>
					<View style={styles.rightSide}>
						<TouchableOpacity style={styles.headerLinkBtn} onPress={this.getContent.bind(this)}>
							<Text style={styles.headerLink}>{this.state.loader ? "LOADER" : "Next"}</Text>
						</TouchableOpacity>
					</View>
				</View>
        <View style={[styles.workingArea]}>
        	<View style={{flex: 1}}>
		        <WebViewQuillEditor
					    ref={component => (this.webViewQuillEditor = component)}
					    getDeltaCallback={this.getDeltaCallback}
					    contentToDisplay={contentToDisplay}
					    onLoad={this.onLoadCallback}
					    backgroundColor="blue"
					    htmlContentToDisplay={contentToDisplay}
					  />
				  </View>
        </View>
  		</View>
		)
	}
}
export default connect(state => ({
}, mapDispatch))(ArticleFirstStep);


const mapDispatch = (dispatch) => {
   const allActionProps = Object.assign({}, dispatch);
   return allActionProps;
}