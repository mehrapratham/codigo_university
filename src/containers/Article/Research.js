import React, { Component } from 'react';
import { Image, View, ScrollView, TouchableOpacity } from 'react-native';
import { Container, Header, Content, List, ListItem, Left, Body, Right, Thumbnail, Text, Icon } from 'native-base';
import styles from '../../styles/styles';
import ResearchList from '../../components/common/ResearchList'
export default class Research extends Component {
  constructor(props){
    super(props);
    this.state = {
      list: [
        {
          name: 'Marion Moore',
          text: 'Student-NITK',
          button: 'CHAT',
          isVar: true,
          isFollow: false
        },
        {
          name: 'Marion Moore',
          text: 'Student-NITK',
          button: 'CHAT',
          isVar: true,
          isFollow: false
        },
        {
          name: 'Marion Moore',
          text: 'Student-NITK',
          button: 'FOLLOWING',
          isVar: false,
          isFollow: false
        },
        {
          name: 'Marion Moore',
          text: 'Student-NITK',
          button: 'FOLLOW',
          isVar: false,
          isFollow: false
        },
        {
          name: 'Marion Moore',
          text: 'Student-NITK',
          button: 'FOLLOW',
          isVar: false,
          isFollow: true
        },
        {
          name: 'Marion Moore',
          text: 'Student-NITK',
          button: 'CHAT',
          isVar: true,
          isFollow: false
        },
        {
          name: 'Marion Moore',
          text: 'Student-NITK',
          button: 'FOLLOWING',
          isVar: false,
          isFollow: false
        },
        {
          name: 'Marion Moore',
          text: 'Student-NITK',
          button: 'FOLLOW',
          isVar: false,
          isFollow: false
        },
        {
          name: 'Marion Moore',
          text: 'Student-NITK',
          button: 'FOLLOW',
          isVar: false,
          isFollow: true
        }
      ]
    }
  }
  render() {
    return (
        <View style={styles.postCon}>
          <View style={styles.postHeader}>
            <TouchableOpacity>
              <Icon name="arrow-back" style={[styles.closeBtn, styles.leftArrow]} />
            </TouchableOpacity>
            <View style={styles.nextToIcon}>
              <Text style={styles.topTitle}>Research</Text>
            </View>
            <View style={styles.rightSide}>
            </View>
          </View>
          <ScrollView>
            <View style={{paddingBottom: 40,backgroundColor: '#fff'}}>
              {this.state.list.map((item,key) => {
                return(
                  <ResearchList list={item} key={key}/>
                )
              })}
            </View>
          </ScrollView>
          <View>
            <TouchableOpacity style={[styles.bottomParticipant,styles.bigLengthBtn]} activeOpacity={0.7}>
              <Text style={[styles.LinkColor, styles.boldText,styles.greenColor,styles.addParticipantText]}>+ Add Participants</Text>
            </TouchableOpacity>
          </View>
        </View>
    );
  }
}
