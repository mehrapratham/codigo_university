import React from 'react'
import { View, Text, TouchableOpacity, Image, Dimensions, TextInput, ScrollView } from 'react-native'
import { Container, Content, Tabs, Tab } from 'native-base';
import styles from '../../styles/styles'
import FeedSearchBox from '../../components/Form/FeedSearchBox'
import { LinearGradient } from 'expo';

export default class RecentSearch extends React.Component{
  render(){
    const { height,width } = Dimensions.get('window');
    return(
      <Container style={{paddingTop: 14}}>
        <View style={[styles.hasFlex]}>
          <View style={[styles.backGradient, {backgroundColor: '#e4e5e7'}]}>
            <FeedSearchBox placeholder="Search"/>
          </View>
          <ScrollView style={styles.hasWhiteBackground}>
            <View style={styles.searchHeading}>
              <Text style={styles.headingTitle}>Recent Search</Text>
              <TouchableOpacity style={styles.clearSearch}>
                <Text style={styles.clearSearchLink}>Clear Search</Text>
              </TouchableOpacity>
            </View>
            <View style={styles.searchList}>
              <View style={styles.searchListCon}>
                <Image source={require('../../img/user_sm.png')} style={styles.searchImg} />
                <Text style={styles.searchText}>Working on web Application</Text>
                <Text style={styles.searchType}>Event</Text>
              </View>
              <View style={styles.searchListCon}>
                <Image source={require('../../img/user_sm.png')} style={styles.searchImg} />
                <Text style={styles.searchText}>Working on web Application</Text>
                <Text style={styles.searchType}>Event</Text>
              </View>
              <View style={styles.searchListCon}>
                <Image source={require('../../img/user_sm.png')} style={styles.searchImg} />
                <Text style={styles.searchText}>Working on web Application</Text>
                <Text style={styles.searchType}>Event</Text>
              </View>
              <View style={styles.searchListCon}>
                <Image source={require('../../img/user_sm.png')} style={styles.searchImg} />
                <Text style={styles.searchText}>Working on web Application</Text>
                <Text style={styles.searchType}>Event</Text>
              </View>
              <View style={styles.searchListCon}>
                <Image source={require('../../img/user_sm.png')} style={styles.searchImg} />
                <Text style={styles.searchText}>Working on web Application</Text>
                <Text style={styles.searchType}>Event</Text>
              </View>
              <View style={styles.searchListCon}>
                <Image source={require('../../img/user_sm.png')} style={styles.searchImg} />
                <Text style={styles.searchText}>Working on web Application</Text>
                <Text style={styles.searchType}>Event</Text>
              </View>
              <View style={styles.searchListCon}>
                <Image source={require('../../img/user_sm.png')} style={styles.searchImg} />
                <Text style={styles.searchText}>Working on web Application</Text>
                <Text style={styles.searchType}>Event</Text>
              </View>
              <View style={styles.searchListCon}>
                <Image source={require('../../img/user_sm.png')} style={styles.searchImg} />
                <Text style={styles.searchText}>Working on web Application</Text>
                <Text style={styles.searchType}>Event</Text>
              </View>
              <View style={styles.searchListCon}>
                <Image source={require('../../img/user_sm.png')} style={styles.searchImg} />
                <Text style={styles.searchText}>Working on web Application</Text>
                <Text style={styles.searchType}>Event</Text>
              </View>
              <View style={styles.searchListCon}>
                <Image source={require('../../img/user_sm.png')} style={styles.searchImg} />
                <Text style={styles.searchText}>Working on web Application</Text>
                <Text style={styles.searchType}>Event</Text>
              </View>
            </View>
          </ScrollView>
        </View>
      </Container>
    )
  }
}
