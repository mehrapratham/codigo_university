import React from 'react'
import { View, Text, Image, TouchableOpacity, ScrollView } from 'react-native'
import { Icon, Tabs, Tab, Accordion } from 'native-base'
import styles from '../../styles/styles';
import { LinearGradient } from 'expo';

export default class Bookmark extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            items: [
                { data: 'Engineering', icon: require('../../img/lock.png') },
                { data: 'Furniture Design', icon: require('../../img/lock.png') },
                { data: 'Design' },
                { data: 'UI UX' }
            ]
        }
    }
    render() {
        return (
            <View style={styles.bkMain}>
                <Text style={styles.bkHeading}>Your Boards</Text>
                <View style={styles.bkImgOut}>
                    {
                        this.state.items.map((val, key) => {
                            return (
                                <View style={styles.bkImg1} key={key}>
                                    <View style={styles.bkImgInner}>
                                        <View style={styles.bkInnerImgOut}>
                                            <Image source={require('../../img/multiImg.png')} style={styles.stydBackImg} />
                                        </View>
                                        <View style={styles.bkInnerImgOut}>
                                            <Image source={require('../../img/article.png')} style={styles.stydBackImg} />
                                        </View>
                                        <View style={styles.bkInnerImgOut}>
                                            <Image source={require('../../img/article1.png')} style={styles.stydBackImg} />
                                        </View>
                                        <View style={styles.bkInnerImgOut}>
                                            <Image source={require('../../img/user.jpg')} style={styles.stydBackImg} />
                                        </View>
                                    </View>
                                    <View style={styles.bkIconOut}>
                                       {val.icon ? <Image style={styles.bkTextIcon} source={val.icon}/> : null} 
                                        <Text style={styles.bkTextHead}>{val.data}</Text>
                                    </View>
                                </View>
                            )
                        })
                    }
                </View>
                <TouchableOpacity activeOpacity={0.5}>
                    <LinearGradient
                        colors={['#58c075', '#4cb9d7']}
                        style={[styles.addGradientButtn, styles.bkfixedPosition]}
                        start={[0.1, 0.4]} end={[1.3, 0.3]}
                    >
                        <Text style={[styles.colorWhite, styles.boldText, styles.fontSize26]}>+</Text>
                    </LinearGradient>
                </TouchableOpacity>
            </View>
        )
    }
}