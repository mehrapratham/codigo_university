import React from 'react'
import { View, Text, Image, TouchableOpacity, ScrollView, TextInput } from 'react-native'
import styles from '../../styles/styles'
import {Icon, Picker,Content} from 'native-base'
import TextEditor from '../../components/Form/TextEditor'
import TextBox from '../../components/Form/TextBox'
import SelectBox from '../../components/Form/SelectBox'
import { Chip, Selectize } from 'react-native-material-selectize';
import ChipsBox from '../../components/Form/ChipsBox'
import {Video} from 'expo'
import VideoPlayer from '@expo/videoplayer';
import FeedActions from '../../components/common/FeedActions'
import AddComment from '../../components/common/AddComment'
import WrittenBy from '../../components/common/WrittenBy'
import FeedUser from '../../components/common/FeedUser'
import CommentSection from '../../components/common/CommentSection'
import CheckBox from 'react-native-check-box'
import { Actions } from 'react-native-router-flux'
import GradientButton from '../../components/Form/GradientButton'

export default class AddNewCardScreen extends React.Component{
	constructor(props){
		super(props)
		this.state = {
			language: 'java',
			isChecked: false
		}
	}
	render(){
		return(
			<View style={styles.postCon}>
					<View style={[styles.postHeader, styles.hasBottomBorder]}>
						<TouchableOpacity>
							<Icon name="arrow-back" style={[styles.closeBtn, styles.leftArrow]} onPress={() => {Actions.pop()}}/>
						</TouchableOpacity>
						<View style={styles.nextToIcon}>
							<Text style={styles.topTitle}>Add New Card - To pay Rs.1200</Text>
						</View>
						<View style={styles.rightSide}>
							
						</View>
					</View>
					<View style={[styles.mainbox,styles.hasWhiteBackground,styles.hasFlex]}>
						<Text style={styles.topTitle}>Add New Card</Text>
						<View style={styles.marginTop15}>
							<TextInput
				        style={styles.textInput}
				        placeholder="Enter the name of your card"
				        placeholderTextColor='#a7b8ca'
				      />
				      <View style={styles.relativeConView}>
				      	<View>
						      <TextInput
						        style={[styles.textInput,{paddingRight: 40}]}
						        placeholder="4242 42424 42424 4242"
						        placeholderTextColor='#a7b8ca'
						      />
						    </View>
						    <View style={styles.creditCon}>
						    	<Image source={require('../../img/card.png')} style={styles.replyImg}/>
						    </View>
					    </View>
					    <View style={styles.replyActionCon}>
					    	<View style={[styles.width50,styles.paddingRight7]}>
					    		<Picker
					    			mode="dropdown"
									  selectedValue={this.state.language}
									  textStyle={styles.pickerTextCard}
									  style={[styles.pickerStyleCard]}
									  onValueChange={(itemValue, itemIndex) => this.setState({language: itemValue})}
									  placeholderIconColor="red">
									  <Picker.Item label="Java" value="java" />
									  <Picker.Item label="JavaScript" value="js" />
									</Picker>
					    	</View>
					    	<View style={[styles.width50,styles.paddingLeft7]}>
					    		<TextInput
						        style={[styles.textInput]}
						        placeholder="CVV"
						        placeholderTextColor='#a7b8ca'
						      />
					    	</View>
					    </View>
					    <View>
					    	<CheckBox
							    style={{flex: 1, padding: 10}}
							    onClick={() =>{
							      this.setState({
							          isChecked:!this.state.isChecked
							      })
							    }}
							    isChecked={this.state.isChecked}
							    rightText={<Text style={[styles.rightTextCheckbox]}>Save card details for future purchase</Text>}
							    style={styles.checkboxStyleCard}
							    unCheckedImage={<Image source={require('../../img/blankcheckbox.png')} style={styles.checkboxImgCard} />}
							    checkedImage={<Image source={require('../../img/checked.png')} style={styles.checkboxImgCard} />}
								/>
								<View style={{marginLeft: 33}}>
									<Text style={styles.manipalText11}>Your payment information is stored securely</Text>
									<Text style={styles.manipalText113}>Learn More</Text>
								</View>
					    </View>
						</View>
					</View>
				<View style={styles.lastBtnGdt}>
					<GradientButton label="ADD CARD"/>
				</View>
      </View>
		)
	}
}
