import React from 'react'
import { View, Text, Image, TouchableOpacity, ScrollView } from 'react-native'
import { Icon, Tabs, Tab, Accordion } from 'native-base'
import styles from '../../styles/styles';
import GradientButton from '../../components/Form/GradientButton';
import Skills from './Skills';
import Bookmark from './Bookmark';

export default class StudentProfile extends React.Component {
    render() {
        return (
            <View style={styles.stdMain}>
                <View style={[styles.postHeader, { backgroundColor: '#fbfbfb' }]}>
                    <TouchableOpacity onPress={() => { Actions.pop() }}>
                        <Icon name="arrow-back" style={styles.stdLeftArrow} />
                    </TouchableOpacity>
                    <View style={styles.nextToIcon}>
                        <Text>Kamila Katherine</Text>
                    </View>
                    <View style={styles.rightSide}>
                        <Icon name="arrow-down" style={styles.stdDownArrow} />
                    </View>
                </View>
                <ScrollView>
                    <View style={styles.stdMainCon}>
                        <View style={styles.stdImgOut}>
                            <Image source={require('../../img/article1.png')} style={styles.stydBackImg} />
                        </View>
                        <View style={styles.stdPrfImgOut}>
                            <Image source={require('../../img/multiImg.png')} style={styles.stdPrfImg} />
                        </View>
                        <View style={styles.stdTextCon}>
                            <Text style={styles.stdTextFirst}>
                                Kamila Katherine
                        </Text>
                            <Text style={styles.stdTextSecond}>
                                Faculty at Loyala Academy
                        </Text>
                            <Text style={styles.stdTextSecond}>
                                Loyala Academy - Hyderabad, India
                        </Text>
                        </View>
                        <View style={styles.stdLine}>

                        </View>
                        <View style={styles.stdTestSecondCon}>
                            <Text style={styles.stdTextSecondDes}>
                                Experince Faculty with a demonstrated history of working in the higher education.
                            <Text style={styles.stdTextLink}>
                                    Show more
                            </Text>
                            </Text>
                        </View>
                        <View style={styles.stdTextFollow}>
                            <Text style={styles.stdTextFollowNumber}>
                                890
                            </Text>
                            <Text style={styles.stdTextSecondDes}>
                                Followers
                            </Text>
                            <Text style={styles.stdTextSecondLine}>
                                |
                              </Text>
                            <Text style={styles.stdTextFollowNumber}>
                                1K
                             </Text>
                            <Text style={styles.stdTextSecondDes}>
                                Following
                            </Text>
                            <Text style={styles.stdTextSecondLine}>
                                |
                            </Text>
                            <View style={styles.StdImgIcon}>
                                <View style={styles.stdImgIconOut}>
                                    <Image source={require('../../img/insta.png')} style={styles.stydBackImg} />
                                </View>
                                <View style={styles.stdImgIconOut}>
                                    <Image source={require('../../img/facebook.png')} style={styles.stydBackImg} />
                                </View>
                                <View style={styles.stdImgIconOut}>
                                    <Image source={require('../../img/twitter.png')} style={styles.stydBackImg} />
                                </View>
                            </View>
                        </View>
                        <View style={styles.stdBtnOut}>
                            <GradientButton label="UPGRADE PROFILE" smallGradientButtn={styles.stdTouchBtn} smallGradientText={styles.stdTouchText} isFollow={false} />
                        </View>
                        <View style={styles.stdLineBtm}>

                        </View>
                    </View>
                    <View style={styles.stdSecondCon}>
                        <Text style={styles.stdSndConText}>
                            Your Profile Performance
                        </Text>
                        <View style={styles.stdSndConLine}>

                        </View>
                        <View style={styles.stdInnerMain}>
                            <TouchableOpacity style={styles.stdInnerCon}>
                                <View style={styles.stdInnerConBorder1}>
                                    <View style={styles.stdInnerTextAligne}>
                                        <Text style={styles.stdInnerNumber}>890</Text>
                                        <Icon name="arrow-forward" style={styles.stdInnerArrowRight} />
                                    </View>
                                    <Text style={styles.stdInnerText}>People viewed your profile</Text>
                                </View>
                            </TouchableOpacity>
                            <TouchableOpacity style={styles.stdInnerCon}>
                                <View style={styles.stdInnerConBorder2}>
                                    <View style={styles.stdInnerTextAligne}>
                                        <Text style={styles.stdInnerNumber}>39</Text>
                                        <Icon name="arrow-forward" style={styles.stdInnerArrowRight} />
                                    </View>
                                    <Text style={styles.stdInnerText}>Search Appearence</Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.stdLineBtm}>

                        </View>
                    </View>
                    <View style={styles.stdThirdCon}>
                        <View style={styles.stdThdConText}>
                            <Text style={styles.stdThdConText1}>Work Experince</Text>
                            <Text style={styles.stdThdConText2}>(12 years)</Text>
                        </View>
                        <View style={styles.stdThdConLine}>

                        </View>
                        <View style={styles.stdTextConInner}>
                            <View style={styles.stdThirdInnerCon}>
                                <View style={styles.stdThirdConImg}>
                                    <Image source={require('../../img/multiImg.png')} style={styles.stydBackImg} />
                                </View>
                                <View >
                                    <Text style={styles.stdThirdConText1}>Faculty</Text>
                                    <Text style={styles.stdThirdConText2}>Loyala Academy</Text>
                                    <Text style={styles.stdThirdConText3}>August 2010- present | 8 years</Text>
                                </View>
                            </View>
                        </View>
                        <View style={styles.stdThdConLine2}>

                        </View>
                        <View style={styles.stdTextConInner}>
                            <View style={styles.stdThirdInnerCon}>
                                <View style={styles.stdThirdConImg}>
                                    <Image source={require('../../img/multiImg.png')} style={styles.stydBackImg} />
                                </View>
                                <View >
                                    <Text style={styles.stdThirdConText1}>Sr. Visual Designer</Text>
                                    <Text style={styles.stdThirdConText2}>Lollypop Design Studio</Text>
                                    <Text style={styles.stdThirdConText2}>August 2010- present | 8 years</Text>
                                </View>
                            </View>
                        </View>
                        <View style={styles.stdThdConLine2}>

                        </View>
                        <Text style={styles.stdThdConTextLast}>
                            View More
                        </Text>
                        <View style={styles.stdLineBtm}>

                        </View>
                    </View>
                    <View>
                        <Tabs style={styles.stdtabStyle} tabContainerStyle={styles.stdTabCon} tabStyle={styles.tabStyle} tabBarUnderlineStyle={styles.tabBarUnderline}>
                            <Tab heading="SKILLS" tabStyle={styles.customTabStyle} activeTabStyle={styles.customTabStyle} activeTextStyle={styles.stdactiveTabText} textStyle={styles.stdtabText} style={styles.atricleTabCon}>
                                <Skills />
                            </Tab>
                            <Tab heading="BOOKMARKS" tabStyle={styles.customTabStyle} activeTabStyle={styles.customTabStyle} activeTextStyle={styles.stdactiveTabText} textStyle={styles.stdtabText} style={styles.atricleTabCon}>
                                <Bookmark />
                            </Tab>
                            <Tab heading="ACTIVITY" tabStyle={styles.customTabStyle} activeTabStyle={styles.customTabStyle} activeTextStyle={styles.stdactiveTabText} textStyle={styles.stdtabText} style={styles.atricleTabCon}>
                            </Tab>
                        </Tabs>
                    </View>
                </ScrollView>
            </View>
        )
    }
}