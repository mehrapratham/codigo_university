import React from 'react'
import { View, Text, Image, TouchableOpacity, ScrollView } from 'react-native'
import styles from '../../styles/styles'
import { Icon, Tabs, Tab, Accordion } from 'native-base'
import TextEditor from '../../components/Form/TextEditor'
import TextBox from '../../components/Form/TextBox'
import SelectBox from '../../components/Form/SelectBox'
import { Chip, Selectize } from 'react-native-material-selectize';
import ChipsBox from '../../components/Form/ChipsBox'
import {Video, MapView} from 'expo'
import VideoPlayer from '@expo/videoplayer';
import FeedActions from '../../components/common/FeedActions'
import AddComment from '../../components/common/AddComment'
import AddCommentTextarea from '../../components/common/AddCommentTextarea'
import WrittenBy from '../../components/common/WrittenBy'
import MemberImages from '../../components/common/MemberImages'
import { Actions } from 'react-native-router-flux'
import GradientButton from '../../components/Form/GradientButton'
import Skills from './Skills';
import FeedUser from '../../components/common/FeedUser'
import AccordionList from '../../components/common/CustomAccordion'

export default class EventScreen extends React.Component {
	constructor(props) {
		super(props)
		this.state = {
			list:[
	      {
	        title: 'Why should you attend?',
	        body: 'React native Accordion/Collapse component, very good to use in toggles & show/hide content'
	      },
	      {
	        title: 'What is the event?',
	        body: 'AccordionList,Collapse,CollapseHeader & CollapseBody'
	      },
	      {
	        title: 'What is this event?',
	        body: 'AccordionList,Collapse,CollapseHeader & CollapseBody'
	      }
	     ],
		}
	}
	_head(item){
    return(
      <View style={styles.accordionHeader}>
        <Text style={styles.accordionHeaderText}>{item.title}</Text>
        
      </View>
    );
	}

	_body(item){
    return (
      <View style={styles.accordionBody}>
        <Text style={styles.accordionBodyText}>{item.body}</Text>
      </View>
    );
	}
	render(){
		return(
			<View style={styles.postCon}>
				<View style={[styles.postHeader, { backgroundColor: '#fbfbfb' }]}>
					<TouchableOpacity onPress={() => { Actions.pop() }}>
						<Icon name="arrow-back" style={[styles.closeBtn, styles.leftArrow]} />
					</TouchableOpacity>
					<View style={styles.nextToIcon}>
						<Text>Workshop on Web App..</Text>
					</View>
					<View style={styles.rightSide}>

					</View>
				</View>
        <View style={[styles.workingArea, styles.darkGray]}>
        	<ScrollView>
        		<View>
        			<Image source={require('../../img/event.png')} style={styles.eventBanner} />
        		</View>
	        	<View style={[styles.formCon, styles.hasWhiteBG, styles.marginBottom20]}>
	        		<Text style={styles.eventHeading}>Marketing and Manage...</Text>
	        		<Text style={styles.eventTime}>17-19th May, Fri-Sun</Text>
	        		<Text style={styles.eventAddress}>Dr. TMA Pai Hall, Manipal University</Text>
	        		<Text style={styles.eventTime}>Rs. 1,200 per person</Text>
	        		<View style={styles.eventBtn}>
		          	<GradientButton label={"BOOK NOW"} />
	            </View>
	        	</View>
	        	<View>
	        		<Tabs style={styles.tabStyle} locked={true} tabContainerStyle={{height: 40}} tabStyle={styles.tabStyle} tabBarUnderlineStyle={styles.tabBarUnderline}>
		            <Tab heading="ABOUT" tabStyle={styles.customTabStyle} activeTabStyle={styles.customTabStyle} activeTextStyle={styles.activeTabText} textStyle={styles.tabText} style={styles.atricleTabCon}>
		              <View style={styles.tabContainer}>
		              	<View style={styles.marginBottom30}>
			            		<Text style={styles.aboutHeading}>About the Event</Text>
			            		<Text style={styles.aboutContent}>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</Text>
			            		<TouchableOpacity>
			            			<Text style={styles.showMore}>Show more</Text>
			            		</TouchableOpacity>
		            		</View>
		            		<View style={styles.marginBottom30}>
		            			<Text style={styles.aboutHeading}>Skills you would bain by being a part of the Event</Text>
		            			<View style={[styles.eventTagsCon]}>
		            				<Text style={styles.eventTag}>HTML and CSS</Text>
		            				<Text style={styles.eventTag}>Javascript</Text>
		            				<Text style={styles.eventTag}>User Interface</Text>
		            				<Text style={styles.eventTag}>Python</Text>
		            				<Text style={styles.eventTag}>Libgdx</Text>
		            			</View>
		            			<GradientButton label={"Join a Skill"} smallGradientText={{color: '#58c176', fontSize: 10, letterSpacing: 0.6}} smallGradientButtn={{width: 100}} customHeight={30} isFollow={true} smallGradientButtn={true} customWidth={100} />
		            			
		            		</View>
		            		<View>
		            			<Text style={[styles.aboutHeading, styles.marginBottom20]}>Speakers and Guests</Text>
		            			<View style={styles.speakersCon}>
		            				<View style={styles.speakerMember}>
		            					<Image source={require('../../img/user.png')} />
		            					<Text style={styles.speakerName}>Manu Kapoor</Text>
		            				</View>
		            				<View style={styles.speakerMember}>
		            					<Image source={require('../../img/user.png')} />
		            					<Text style={styles.speakerName}>Manu Kapoor</Text>
		            				</View>
		            				<View style={styles.speakerMember}>
		            					<Image source={require('../../img/user.png')} />
		            					<Text style={styles.speakerName}>Manu Kapoor</Text>
		            				</View>
		            				<View style={styles.speakerMember}>
		            					<Image source={require('../../img/user.png')} />
		            					<Text style={styles.speakerName}>Manu Kapoor</Text>
		            				</View>
		            			</View>
		            		</View>

		            		<View style={styles.marginBottom30}>
		            			<Text style={[styles.aboutHeading]}>Event Location</Text>
		            			<Text style={styles.aboutContent}>Dr. TMA Pai Hall, Manipal University</Text>
		            			<View style={styles.mapViewCon}>
			            			<MapView
									        style={styles.mapView}
									        initialRegion={{
									          latitude: 37.78825,
									          longitude: -122.4324,
									          latitudeDelta: 0.0922,
									          longitudeDelta: 0.0421,
									        }}
									      />
								      </View>
		            		</View>

		            		<View style={styles.marginBottom20}>
		            			<Text style={[styles.aboutHeading]}>Media</Text>
		            			<ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
		            				<View style={styles.mediaImgCon}>
			            				<View style={styles.mediaImg}>
			            					<Image source={require('../../img/media.png')} style={styles.ImgFullWidth} />
			            				</View>
			            				<View style={styles.mediaImg}>
			            					<Image source={require('../../img/media.png')} style={styles.ImgFullWidth} />
			            				</View>
			            				<View style={styles.mediaImg}>
			            					<Image source={require('../../img/media.png')} style={styles.ImgFullWidth} />
			            				</View>
		            				</View>
		            			</ScrollView>
		            		</View>

		            	</View>
		            </Tab>
		            <Tab heading="SKILLS" tabStyle={styles.customTabStyle} activeTabStyle={styles.customTabStyle} activeTextStyle={styles.activeTabText} textStyle={styles.tabText} style={styles.atricleTabCon}>
						<Skills />
		            </Tab>
		            <Tab heading="DISCUSSIONS" tabStyle={styles.customTabStyle} activeTabStyle={styles.customTabStyle} activeTextStyle={styles.activeTabText} textStyle={styles.tabText} style={styles.atricleTabCon}>
		            	<View style={{backgroundColor: '#e5e5e5'}}>
			            	<View style={styles.discissionCon}>
			            		<AddCommentTextarea />
			            	</View>

			            	<View>
			            		<View style={styles.feedCon}>
												<View style={styles.feedHeader}>
													<View style={styles.marginBottom20}>
														<FeedUser />
													</View>
													<View>
														<TouchableOpacity>
															<Text><Text style={styles.userfeedColor}>MRI-Safe Robots treat Epilepsy: </Text><Text style={styles.userfeedLink}>http://www.google.com reminds me of this.</Text></Text>
														</TouchableOpacity>
													</View>
												</View>
												<TouchableOpacity onPress={() =>{Actions.FeedDetail()}} activeOpacity={0.5}>
						        			<Image source={require('../../img/feed.png')} style={styles.feedImg} />
												</TouchableOpacity>
												<View>
													<FeedActions onLikePress={() => {Actions.Likes()}}/>
												</View>
												<View>
													<AddComment />
												</View>
											</View>
											<View style={styles.feedCon}>
												<View style={styles.feedHeader}>
													<View style={styles.marginBottom20}>
														<FeedUser />
													</View>
													<View>
														<Text style={styles.videoText}>This Video explain fluid statics and dynamics in detail.</Text>
													</View>
												</View>
												<View>
						        			<VideoPlayer
						               	videoProps={{
						                 	shouldPlay: false,
						                 	resizeMode: Video.RESIZE_MODE_CONTAIN,
						                 	source: {
						                   	uri: 'https://bitdash-a.akamaihd.net/content/sintel/hls/playlist.m3u8',
						                 	},
						               	}}
						                isPortrait={true}
						                playFromPositionMillis={0}
						              />
												</View>
												<View>
													<FeedActions />
												</View>
												<View>
													<AddComment />
												</View>
											</View>
			            	</View>
		            	</View>
		            </Tab>
		            <Tab heading="FAQS" tabStyle={styles.customTabStyle} activeTabStyle={styles.customTabStyle} activeTextStyle={styles.activeTabText} textStyle={styles.tabText} style={styles.atricleTabCon}>
		            	<View style={styles.tabContainer}>
		            		<Text style={styles.faqHeading}>Frequently Asked Questions</Text>
		            		<AccordionList
					            list={this.state.list}
					            header={this._head}
					            body={this._body}
					          />
		            	</View>
		            </Tab>
		          </Tabs>
	        	</View>
        	</ScrollView>
        </View>
      </View>
		)
	}
}
