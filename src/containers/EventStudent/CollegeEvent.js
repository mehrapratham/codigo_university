import React from 'react'
import { View, Text, Image, TouchableOpacity, ScrollView } from 'react-native'
import styles from '../../styles/styles'
import {Icon} from 'native-base'
import { Container, Tabs, Tab } from 'native-base';
import FeedSearchBox from '../../components/Form/FeedSearchBox'
import SearchPost from '../../components/Search/SearchPost'
import SearchMember from '../../components/Search/SearchMember'
import EventCollegesList from '../../components/common/Lists/EventCollegesList'

export default class CollegeEvent extends React.Component{
	constructor(props){
		super(props)
		this.state = {
			list: [
				{
					img: ''
				},
				{
					img: ''
				},
				{
					img: ''
				},
				{
					img: ''
				},
				{
					img: ''
				},
				{
					img: ''
				},
				{
					img: ''
				},
				{
					img: ''
				},
				{
					img: ''
				}
			]
		}
	}
	render(){
		return(
			<Container style={{paddingTop: 24}}>
        <View style={[styles.hasFlex]}>
          <View style={[styles.backGradient, {backgroundColor: '#e4e5e7'}]}>
            <FeedSearchBox placeholder="Market" arrowBack={true} />
          </View>
          <ScrollView>
	          <View style={[styles.searchFoundCon,{backgroundColor: '#fafbfc'}]}>
	        		<Text style={styles.searchFound}>You Found </Text>
	        		<Text style={styles.searchFoundBold}>3,212 </Text>
	        		<Text style={styles.searchFound}>results for </Text>
	        		<Text style={styles.searchFoundBold}>"Madhavi"</Text>
	        	</View>
	      		<View style={[styles.searchHeading, {backgroundColor: '#fafbfc'}]}>
	      			<View style={styles.errorCon}>
	      				<View style={[styles.heightWidth20,styles.marginRight5]}>
	      					<Image source={require('../../img/hut.png')} style={[styles.replyImg]}/>
	      				</View>
	      				<Text style={styles.collegeTitle}>Colleges</Text>
	      			</View>
	            <TouchableOpacity style={styles.clearSearch}>
	              <Text style={styles.clearSearchLink}>Filter</Text>
	            </TouchableOpacity>
	          </View>
          	<View>
	          	{this.state.list.map((item,key) => {
	          		return(
	          			<EventCollegesList list={item} key={key}/>
	          		)
	          	})}
	          </View>
          </ScrollView>
        </View>
      </Container>
		)
	}
}
