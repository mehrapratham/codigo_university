import React from 'react'
import { View, Text, Image, TouchableOpacity, ScrollView } from 'react-native'
import styles from '../../styles/styles'
import { Tabs, Tab, Icon } from 'native-base';
import EventCardList from '../../components/common/Lists/EventCardList'

export default class Events extends React.Component{
	constructor(props){
		super(props)
		this.state = {
			list: [
				{
					img: ''
				},
				{
					img: ''
				},
				{
					img: ''
				},
				{
					img: ''
				},
				{
					img: ''
				},
				{
					img: ''
				},
				{
					img: ''
				},
				{
					img: ''
				},
				{
					img: ''
				}
			]
		}
	}
	render(){
		return(
			<View style={styles.postCon}>
				<View style={[styles.postHeader, styles.hasBorderBottom]}>
					<TouchableOpacity>
						<Icon name="arrow-back" style={[styles.closeBtn, styles.leftArrow]} />
					</TouchableOpacity>
					<View style={styles.nextToIcon}>
						<Text style={styles.topTitle}>Events</Text>
					</View>
					<View style={styles.rightSide}>
					</View>
				</View>
        <View style={styles.workingArea}>
        	<Tabs style={styles.tabStyle} tabContainerStyle={{height: 40}} tabStyle={styles.tabStyle} tabBarUnderlineStyle={styles.tabBarUnderline}>
            <Tab heading="ONGOING" tabStyle={styles.customTabStyle} activeTabStyle={styles.customTabStyle} activeTextStyle={styles.activeTabText} textStyle={styles.tabText} style={styles.atricleTabCon}>
              	<ScrollView showsVerticalScrollIndicator={false}>
              		<View style={styles.tabOuterBoxEventList}>
              			{this.state.list.map((item,key) => {
              				return(
              					<EventCardList list={item} key={key}/>
              				)
              			})}
	              	</View>
              	</ScrollView>          
            </Tab>
            <Tab heading="COMPLETED" tabStyle={styles.customTabStyle} activeTabStyle={styles.customTabStyle} activeTextStyle={styles.activeTabText} textStyle={styles.tabText} style={styles.atricleTabCon}>
            	<ScrollView showsVerticalScrollIndicator={false}>
              		<View style={styles.tabOuterBoxEventList}>
              			{this.state.list.map((item,key) => {
              				return(
              					<EventCardList list={item} key={key}/>
              				)
              			})}
	              	</View>
              	</ScrollView> 
            </Tab>
          </Tabs>
        </View>
      </View>
		)
	}
}
