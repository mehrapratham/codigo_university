import React from 'react'
import { View, Text, Image, ImageBackground } from 'react-native'
import { connect } from 'react-redux'
import { Actions } from 'react-native-router-flux'
import styles from '../../styles/styles1'
class Splash extends React.Component{
	componentWillMount(){
		setTimeout(function(){
			Actions.StartScreen()
    	}, 2000);
	}
	render(){
		return(
			
			<ImageBackground source={require('../../img/room.jpeg')} style={{flex: 1}}>
				<View style={{flex: 1,alignItems: 'center',justifyContent: 'center'}}>
					<Text style={{fontSize: 26,color: '#fff',fontWeight: '700'}}>UNIVERSITY.SOCIAL</Text>
				</View>
        	</ImageBackground>
		)
	}
}
export default connect(state => ({}, mapDispatch))(Splash);

const mapDispatch = (dispatch) => {
   const allActionProps = Object.assign({}, dispatch);
   return allActionProps;
}