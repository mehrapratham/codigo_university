import React from 'react'
import { View, Text, TouchableOpacity } from 'react-native'
import TopHeader from '../../components/common/header'
import TextBox from '../../components/Form/TextBox'
import { Container, Content, Button } from 'native-base';
import GradientButton from '../../components/Form/GradientButton'
import styles from '../../styles/styles'
import ProgressBar from '../../components/common/ProgressBar'
import {RadioGroup, RadioButton} from 'react-native-flexi-radio-button'
import RadioBtn from '../../components/Form/RadioBtn'
import { Actions } from 'react-native-router-flux'

export default class ResetPassword extends React.Component{
    render(){
        return(
            <Container style={styles.topBox}>
                <TopHeader title="Reset Password" />
                <Content>
                    <View style={styles.mainbox}>
                        
                        <View>
                            <TextBox label="Enter new password" />
                              <TextBox label="Confirm Password"  secureTextEntry={true} />
                        </View>
                        <View style={styles.marginBottom30}>
                            <Text style={styles.btmHeading}>Your password needs to include</Text>
                            <View style={styles.upperDotViewStyle}><Text style={styles.textWithBullet}>at least one number or symbol.</Text><Text style={styles.dotStyle}></Text></View>
                            <View style={styles.upperDotViewStyle}><Text style={styles.textWithBullet}>both lower and upper case latin characters.</Text><Text style={styles.dotStyle}></Text></View>
                            <View style={styles.upperDotViewStyle}><Text style={styles.textWithBullet}>have atleast minimum of 8 characters.</Text><Text style={styles.dotStyle}></Text></View>
                        </View>
                        <View>
                              <GradientButton label="DONE" onPress={() => {Actions.Login()}}/>
                        </View>
                     </View>
                </Content>
            </Container>
        )
    }
}