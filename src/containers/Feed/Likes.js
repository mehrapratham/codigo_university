import React, { Component } from 'react';
import { Image, View, ScrollView, TouchableOpacity } from 'react-native';
import { Container, Header, Content, List, ListItem, Left, Body, Right, Thumbnail, Text, Icon } from 'native-base';
import styles from '../../styles/styles';
import LikesList from '../../components/common/LikesList'
import { Actions } from 'react-native-router-flux'
export default class Likes extends Component {
  constructor(props){
    super(props);
    this.state = {
      list: [
        {
          name: 'Marion Moore',
          text: 'Student-NITK',
          button: 'CHAT',
          isVar: true,
          isFollow: false
        },
        {
          name: 'Marion Moore',
          text: 'Student-NITK',
          button: 'CHAT',
          isVar: true,
          isFollow: false
        },
        {
          name: 'Marion Moore',
          text: 'Student-NITK',
          button: 'FOLLOWING',
          isVar: false,
          isFollow: false
        },
        {
          name: 'Marion Moore',
          text: 'Student-NITK',
          button: 'FOLLOW',
          isVar: false,
          isFollow: true
        },
        {
          name: 'Marion Moore',
          text: 'Student-NITK',
          button: 'FOLLOW',
          isVar: false,
          isFollow: true
        },
        {
          name: 'Marion Moore',
          text: 'Student-NITK',
          button: 'FOLLOWING',
          isVar: false,
          isFollow: false
        },
        {
          name: 'Marion Moore',
          text: 'Student-NITK',
          button: 'FOLLOW',
          isVar: false,
          isFollow: true
        },
        {
          name: 'Marion Moore',
          text: 'Student-NITK',
          button: 'FOLLOWING',
          isVar: false,
          isFollow: false
        },
        {
          name: 'Marion Moore',
          text: 'Student-NITK',
          button: 'FOLLOW',
          isVar: false,
          isFollow: true
        },
        {
          name: 'Marion Moore',
          text: 'Student-NITK',
          button: 'FOLLOWING',
          isVar: false,
          isFollow: false
        },
        {
          name: 'Marion Moore',
          text: 'Student-NITK',
          button: 'FOLLOW',
          isVar: false,
          isFollow: true
        },
        {
          name: 'Marion Moore',
          text: 'Student-NITK',
          button: 'FOLLOWING',
          isVar: false,
          isFollow: false
        },
        {
          name: 'Marion Moore',
          text: 'Student-NITK',
          button: 'FOLLOW',
          isVar: false,
          isFollow: true
        }
      ]
    }
  }
  render() {
    return (
      <Container>
        <Header style={styles.whiteColor}>
        <Left>
	        <View style={styles.feedActionCon}>
	          	<TouchableOpacity onPress={() => {Actions.pop()}}>
	            	<Icon name="arrow-back" style={[styles.greenColor,styles.fontSize26]}/>
	            </TouchableOpacity>
	            <Text style={[styles.marginLeft20,styles.fontSize16,styles.checkboxTitle,styles.boldText,styles.darkBlueColor]}>Likes</Text>
	        </View>
        </Left>
        </Header>
        <Content>
          <View> 
            <List>
              {this.state.list.map((item,key) => {
                return(
                  <LikesList list={item} key={key} /> 
                  )
                })
              }
            </List>
          </View>
        </Content>
      </Container>
    );
  }
}
