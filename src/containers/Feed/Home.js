import React from 'react'
import { View, Text, TouchableOpacity, Image, Dimensions, TextInput, ScrollView } from 'react-native'
import { Container, Content, Tabs, Tab } from 'native-base';
import styles from '../../styles/styles'
import FeedSearchBox from '../../components/Form/FeedSearchBox'
import { LinearGradient } from 'expo';
import Feed from './Feed'

export default class Home extends React.Component{
  render(){
    const { height,width } = Dimensions.get('window');
    return(
      <Container style={{paddingTop: 24}}>
        <View style={[styles.hasGrayBackground,styles.hasFlex]}>
          <LinearGradient
            colors={['#6373e9', '#429fec']}
            style={[styles.backGradient]}
            start={[0.1, 1.0]} end={[1.3, 0.3]}
          >
            <FeedSearchBox placeholder="Search"/>
          </LinearGradient>
          <Tabs style={styles.colorWhite} locked={true} tabStyle={styles.colorWhite} tabBarUnderlineStyle={styles.tabBarUnderline}>
            <Tab heading="FEED" style={styles.colorWhite} tabStyle={styles.customTabStyle} activeTabStyle={styles.customTabStyle} activeTextStyle={styles.activeTabText} textStyle={styles.tabText}>
              <Feed />
            </Tab>
            <Tab heading="EVENTS" tabStyle={styles.customTabStyle} activeTabStyle={styles.customTabStyle} activeTextStyle={styles.activeTabText} textStyle={styles.tabText}>
              <Text>Comming soon</Text>
            </Tab>
            <Tab heading="PORTFOLIO" tabStyle={styles.customTabStyle} activeTabStyle={styles.customTabStyle} activeTextStyle={styles.activeTabText} textStyle={styles.tabText}>
              <Text >Comming soon</Text>
            </Tab>
          </Tabs>
        </View>
      </Container>
    )
  }
}
