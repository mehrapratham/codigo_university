import React, { Component } from 'react';
import { Image, View } from 'react-native';
import { Container, Header, Content, List, ListItem, Left, Body, Right, Thumbnail, Text } from 'native-base';
import styles from '../../styles/styles';
import NotificationList from '../../components/common/NotificationList'
export default class Notifications extends Component {
  constructor(props){
    super(props);
    this.state = {
      list: [
        {
          name: 'New',
          lists: [
            {
              text: 'Radhika Vaz and 4 others likes your post',
              time: 'about an hour ago'
            },
            {
              text: 'Radhika Vaz and 4 others likes your post',
              time: 'about an hour ago'
            },
            {
              text: 'Radhika Vaz and 4 others likes your post',
              time: 'about an hour ago'
            },
            
          ]
        },
        {
          name: 'Earlier',
          lists: [
            {
              text: 'Radhika Vaz and 4 others likes your post',
              time: 'about an hour ago'
            },
            {
              text: 'Radhika Vaz and 4 others likes your post',
              time: 'about an hour ago'
            },
            {
              text: 'Radhika Vaz and 4 others likes your post',
              time: 'about an hour ago'
            },
            {
              text: 'Radhika Vaz and 4 others likes your post',
              time: 'about an hour ago'
            },
            {
              text: 'Radhika Vaz and 4 others likes your post',
              time: 'about an hour ago'
            },
            {
              text: 'Radhika Vaz and 4 others likes your post',
              time: 'about an hour ago'
            },
          ]
        }
      ]
    }
  }
  render() {
    return (
      <Container style={styles.topView}>
        <Content>
          <View> 
            <List>
              {this.state.list.map((item,key) => {
                return(
                  <View key={key}>
                    <View style={styles.upperViewNotification}>
                      <Text style={styles.notifyHeading}>{item.name}</Text>
                    </View>
                    {item.lists.map((userList,index) => {
                      return(
                        <NotificationList list={userList} key={index}/>
                      )
                    })}
                  </View>
                )
              })}
              
            </List>
          </View>
        </Content>
      </Container>
    );
  }
}
