import React from 'react'
import { View, Text, Image, TouchableOpacity, ScrollView } from 'react-native'
import styles from '../../styles/styles'
import {Icon} from 'native-base'
import TextEditor from '../../components/Form/TextEditor'
import TextBox from '../../components/Form/TextBox'
import SelectBox from '../../components/Form/SelectBox'
import { Chip, Selectize } from 'react-native-material-selectize';
import ChipsBox from '../../components/Form/ChipsBox'
import {Video} from 'expo'
import VideoPlayer from '@expo/videoplayer';
import FeedActions from '../../components/common/FeedActions'
import AddComment from '../../components/common/AddComment'
import WrittenBy from '../../components/common/WrittenBy'
import FeedUser from '../../components/common/FeedUser'
import CommentSection from '../../components/common/CommentSection'
import { Actions } from 'react-native-router-flux'

export default class FeedDetail extends React.Component{
	constructor(props){
		super(props)
		this.state = {
			currentTags: ['Writing', 'Marketing'],
			participants: ['Rupinderpal', 'Pratham']
		}
	}
	render(){
		return(
			<View style={styles.postCon}>
				<View style={[styles.postHeader, styles.hasBottomBorder]}>
					<TouchableOpacity>
						<Icon name="arrow-back" style={[styles.closeBtn, styles.leftArrow]} onPress={() => {Actions.pop()}}/>
					</TouchableOpacity>
					<View style={styles.nextToIcon}>
						{/*<Text>Articles</Text>*/}
					</View>
					<View style={styles.rightSide}>
						
					</View>
				</View>
        <View style={[styles.workingArea]}>
        	<ScrollView>
	        	<View style={[styles.feedCon]}>
							<View style={styles.feedHeader}>
								<View style={styles.marginBottom20}>
									<FeedUser />
								</View>
								<View>
									<TouchableOpacity>
										<Text>MRI-Safe Robots treat Epilepsy: http://www.google.com reminds me of this.</Text>
									</TouchableOpacity>
								</View>
							</View>
							<View>
	        			<Image source={require('../../img/feed.png')} style={styles.feedImg} />
							</View>
							<View>
								<FeedActions />
							</View>
							<View>
								<CommentSection showDetail={true} />
								<CommentSection />
								<CommentSection />
							</View>
							<View>
								<AddComment />
							</View>
						</View>
        	</ScrollView>
        </View>
      </View>
		)
	}
}
