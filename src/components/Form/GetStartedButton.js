import React from 'react'
import { StyleSheet, TouchableOpacity, Text } from 'react-native'
export default class GetStartedButton extends React.Component{
	render(){
		return(
			<TouchableOpacity style={styles.buttn} onPress={this.props.onPress}>
				<Text>GET STARTED</Text>
			</TouchableOpacity>
		)
	}
}
const styles = StyleSheet.create({
	buttn: {
		height: 45,
		backgroundColor: '#fff',
		alignItems: 'center',
		justifyContent: 'center',
		width: '100%'
	}
})