import React from 'react';
import { Container, Header, Content, Button, Text } from 'native-base';
import styles from '../../styles/styles'
export default class BlockButton extends React.Component{
	render(){
		return(
			<Button block style={styles.blockBtn} onPress={this.props.onPress} disabled={this.props.disabled}>
      	<Text style={styles.blockBtnText}>{this.props.label}</Text>
    	</Button>
		)
	}
}