import React from 'react'
import { View, Text, TextInput, TouchableOpacity } from 'react-native'
import {Icon} from 'native-base'
import styles from '../../styles/styles'

export default class TextBox extends React.Component{
	render(){
		return(
			<View style={[styles.boxCon, this.props.width && {width: this.props.width}, this.props.marginBottom && {marginBottom: this.props.marginBottom}]}>
        <Text style={[styles.boxlabel, {color: this.props.labelColor}]}>
        	{this.props.label}
        	{this.props.required && <Text style={styles.requiredTextSign}>*</Text>}
        </Text>
        {this.props.leftText && <Text style={[styles.leftText, {color: this.props.labelColor}]}>{this.props.leftText}</Text>}
        {this.props.rightLink && <TouchableOpacity style={styles.leftText}><Text style={[styles.leftText, {color: this.props.labelLinkColor}]}>{this.props.rightLink}</Text></TouchableOpacity>}
        <TextInput style={this.props.hasWhitebg ? [styles.textboxWhitebg, {paddingLeft: this.props.leftPadding, paddingRight: this.props.leftPadding, height: this.props.height}] : [styles.textbox, {paddingLeft: this.props.leftPadding}]} keyboardType={this.props.keyboardType} secureTextEntry={this.props.secureTextEntry} underlineColorAndroid="transparent" placeholder={this.props.placeholder} onChangeText={(text) => this.props.onchange(text)} multiline={this.props.multiline} />
        {this.props.icon && <Icon name={this.props.icon} style={styles.textBoxIcon} />}
      </View>
		)
	}
}