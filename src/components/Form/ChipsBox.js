import React from 'react'
import { View, Text, Image, TouchableOpacity } from 'react-native'
import {Icon} from 'native-base'
import CheckBox from 'react-native-check-box'
import styles from '../../styles/styles'
import AutoSuggest from 'react-native-autosuggest';

export default class ChipsBox extends React.Component{
	constructor(props){
		super(props)
		this.state = {
			
		}
	}

	onItemPress(){
		console.log('hi')
	}

	render(){
		return(
			<View style={[styles.chipOuter, this.props.onlyTags && styles.onlyTags]}>
				<View style={[styles.boxCon, {marginBottom: this.props.marginBottom}]}>
	        <Text style={styles.boxlabel}>{this.props.label}</Text>
	        {this.props.leftText && <Text style={styles.leftText}>{this.props.leftText}</Text>}
	        <View style={[styles.chipsBox, this.props.onlyTags && styles.onlyTagsBox]}>
						{this.props.chips.map((item, key) =>{
							return <View style={styles.chip} key={key}>
											<Text style={styles.chipText}>{item}</Text>
											<TouchableOpacity style={styles.chipClose}>
												<Icon name="close" style={styles.chipCloseBtn} />
											</TouchableOpacity>
										</View>
						})}
						{!this.props.onlyTags && 
							<AutoSuggest
					      onChangeText={(text) => console.log('input changing!')}
					      placeholder={this.props.inputPlaceholder}
					      containerStyles={styles.chipsCompleteTextbox}
					      rowWrapperStyles={styles.chipsdropDownStyle}
					      textInputStyles={styles.chipsautoCompleteInput}
					      onItemPress={this.onItemPress.bind(this)}
					      terms={['Apple', 'Banana', 'Orange', 'Strawberry', 'Lemon', 'Cantaloupe', 'Peach', 'Mandarin', 'Date', 'Kiwi']}
					    />
						}
					</View>
	      </View>
	    </View>

		)
	}
}
