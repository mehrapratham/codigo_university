import React from 'react'
import { View, Text, Image } from 'react-native'
import CheckBox from 'react-native-check-box'
import styles from '../../styles/styles'

export default class Checkbox extends React.Component{
	constructor(props){
		super(props)
		this.state = {
			isChecked: false
		}
	}
	render(){
		let rightText = this.props.subTitle ? <Text><Text style={[styles.checkboxTitle,styles.darkBlueColor,styles.fontSize14]}>{this.props.label}</Text><Text style={[styles.subTitle]}>{"\n"}{this.props.subTitle}</Text></Text> : <Text style={[styles.darkBlueColor,styles.fontSize14]}>{this.props.label}</Text>
		return(
			<View>
				<CheckBox
			    style={{flex: 1, padding: 10}}
			    onClick={() =>{
			      this.setState({
			          isChecked:!this.state.isChecked
			      })
			    }}
			    isChecked={this.state.isChecked}
			    rightText={rightText}
			    style={styles.checkboxStyle}
			    unCheckedImage={<Image source={require('../../img/unchecked.png')} style={styles.checkboxImg} />}
			    checkedImage={<Image source={require('../../img/checked.png')} style={styles.checkboxImg} />}
				/>
			</View>
		)
	}
}
