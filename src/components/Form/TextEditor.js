import React from 'react'
import { View, Text, Image, TouchableOpacity } from 'react-native'
import {Icon} from 'native-base'
import styles from '../../styles/styles'
// import {WebViewQuillEditor, WebViewQuillViewer} from 'react-native-webview-quilljs'
import { QuillDeltaToHtmlConverter } from 'quill-delta-to-html';

const contentToDisplay = "<p>test</p>"

export default class TextEditor extends React.Component{
	onLoadCallback(){
		// console.log('here')
	}
	getDeltaCallback = (delta) => {
      // console.log('--------------------');
      // console.log(delta.delta.ops, 321);
      var cfg = {};
      var converter = new QuillDeltaToHtmlConverter(delta.delta.ops, cfg);
      var html = converter.convert(); 
      // console.log(html)
      this.props.submitContent(html)
      // console.log('--------------------');
  }
	getContent(){
		// console.log('here')
		let data = this.webViewQuillEditor.getDelta();
	}

	render(){
		return(
			<View style={{flex: 1}}>
        <View style={{flex: 1}}>
	        {/*<WebViewQuillEditor
				    ref={component => (this.webViewQuillEditor = component)}
				    getDeltaCallback={this.props.getDeltaCallback}
				    contentToDisplay={contentToDisplay}
				    onLoad={this.onLoadCallback}
				    backgroundColor="blue"
				    htmlContentToDisplay={contentToDisplay}
				  />*/}
			  </View>
			  <TouchableOpacity style={[styles.textEditorBtn, {backgroundColor: 'red', zIndex: 10}]} onPress={this.getContent.bind(this)}><Text style={styles.textEditorBtnText}>Next</Text></TouchableOpacity>
      </View>
		)
	}
}