import React from 'react'
import { View, Text, Image } from 'react-native'
import CheckBox from 'react-native-check-box'
import styles from '../../styles/styles'

export default class CheckboxGreen extends React.Component{
	constructor(props){
		super(props)
		this.state = {
			isChecked: false
		}
	}
	render(){
		let rightText = <Text style={[styles.greenColor, this.props.textColor && {color: this.props.textColor}]}>{this.props.label}</Text>
		return(
			<View>
				<CheckBox
			    onClick={() =>{
			      this.setState({
			          isChecked:!this.state.isChecked
			      })
			    }}
			    isChecked={this.state.isChecked}
			    rightText={rightText}
			    style={styles.checkboxStyleGreen}
			    unCheckedImage={<Image source={require('../../img/greenCheckbox.png')} style={styles.checkboxImgGreen} />}
			    checkedImage={<Image source={require('../../img/greenCheckboxChecked.png')} style={styles.checkboxImgGreen} />}
				/>
			</View>
		)
	}
}
