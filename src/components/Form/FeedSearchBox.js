import React from 'react'
import { View, Text, TextInput, TouchableOpacity } from 'react-native'
import {Icon} from 'native-base'
import styles from '../../styles/styles'
import { Actions } from 'react-native-router-flux'
export default class FeedSearchBox extends React.Component{
	goToSearchPage(){
		console.log('here')
		Actions.SearchResult()
	}
	goToFeed(){
		Actions.Feed()
	}

	render(){
		return(
			<View style={styles.feedSerachView}>
        <TextInput style={styles.feedTextbox} onFocus={this.goToSearchPage.bind(this)} keyboardType={this.props.keyboardType} secureTextEntry={this.props.secureTextEntry} underlineColorAndroid="transparent" placeholder={this.props.placeholder} />
        {!this.props.arrowBack && <Icon name="search" style={styles.feedSearchIcon}/>}
        {this.props.arrowBack && <TouchableOpacity style={[styles.feedSearchIcon, {top: 18}]} onPress={this.goToFeed.bind(this)}><Icon name="arrow-back" style={[styles.arrowBackIcon]}/></TouchableOpacity>}
        <Icon name="md-mic" style={styles.feedVoiceIcon}/>
      </View>
		)
	}
}
