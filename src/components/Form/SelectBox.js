import React from 'react'
import { View, Text, TextInput, Image } from 'react-native'
import {Icon, Picker} from 'native-base'
import styles from '../../styles/styles'

export default class SelectBox extends React.Component{
	constructor(props){
		super(props)
		this.state = {
			language: 'java'
		}
	}
	render(){
		return(
			<View style={[styles.boxCon, {marginBottom: this.props.marginBottom}]}>
        <Text style={[styles.boxlabel, {color: this.props.labelColor}]}>{this.props.label}</Text>
        {this.props.leftText && <Text style={styles.leftText}>{this.props.leftText}</Text>}
        <View style={this.props.hasWhitebg ? [styles.textboxWhitebg, {padding: 0, width: this.props.width, height: this.props.height}] : [styles.textbox, {padding: 0, width: this.props.width, height: this.props.height}]}>
        	<Picker
					  selectedValue={this.props.selectedValue}
					  textStyle={styles.pickerText}
					  style={[styles.pickerStyle, {height: this.props.height}]}
					  onValueChange={this.props.onValueChange}
					  placeholder={this.props.placeholder}>
					  {this.props.data && this.props.data.map((item, index) => {
					  	return <Picker.Item label={item[this.props.labelProp]} value={item[this.props.valueProp]} />
					  })}
					</Picker>
					
					<Image source={require('../../img/selectArrow.png')} style={[styles.selectArrow, {top: this.props.height/2 - 4}]} />
        </View>
        
        {this.props.icon && <Icon name={this.props.icon} style={styles.textBoxIcon} />}
      </View>
		)
	}
}