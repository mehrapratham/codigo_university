import React from 'react'
import { View, Text } from 'react-native'
import { Header, Button, Left, Body, Right, Icon, Title } from 'native-base'
import styles from '../../styles/styles'
import { LinearGradient } from 'expo';
import { Actions } from 'react-native-router-flux'

export default class TopHeader extends React.Component{
	render(){
		return(
			<LinearGradient
        colors={['#6c6cf1', '#42a5f7']}
        start={[0.1, 0.1]} end={[1.3, 0.3]}
      >
			<Header style={styles.headerBgColor}>
        <Left>
          <Button transparent style={styles.headerBtn} onPress={() => {Actions.pop()}}>
            <Icon name='arrow-back' style={styles.headerBtnIcon} />
          </Button>
        </Left>
        <Body>
          <Title style={styles.headertext}>{this.props.title}</Title>
        </Body>
        <Right />
      </Header>
      </LinearGradient>
		)
	}
}