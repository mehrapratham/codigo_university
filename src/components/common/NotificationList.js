import React, { Component } from 'react';
import { Image } from 'react-native'
import { Container, Header, Content, List, ListItem, Left, Body, Right, Thumbnail, Text } from 'native-base';
import styles from '../../styles/styles'
export default class NotificationList extends Component {
  render() {
    return (
      <ListItem avatar noIndent style={[styles.lightGreyBackground,styles.paddingTop0,styles.listBorder]} noBorder>
        <Left>
          <Thumbnail source={require('../../img/user.png')} style={styles.thumbnailStyle}/>
        </Left>
        <Body>
          <Text style={styles.marginTop7}><Text note style={[styles.notificationUpperText,styles.marginTop10]}><Text style={styles.notifyName}>Radhika Vaz</Text> and 4 others <Text style={styles.notifyName}>likes </Text>your post</Text></Text>
          <Text note style={[styles.notificationText,styles.marginTop3]}>{this.props.list.time}</Text>
        </Body>
      </ListItem>
    );
  }
}
