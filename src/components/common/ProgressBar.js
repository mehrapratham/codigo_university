import React from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native'
import { LinearGradient } from 'expo';
import styles from '../../styles/styles'

export default class ProgressBar extends React.Component{
  render(){
    let activeProgress = (100/this.props.totalStep)
    activeProgress = activeProgress*this.props.currentStep + '%'
    return(
      <View style={styles.progressBar}>
        <View style={[styles.progressBarActive,{width: activeProgress}]}></View>
      </View>
    )
  }
}