import React from 'react'
import { View, Text, Image, TouchableOpacity } from 'react-native'
import styles from '../../styles/styles'
import FeedUser from './FeedUser'
export default class JobCard extends React.Component{
	render(){
		return(
			<View style={[styles.jobCard, {width: this.props.width}]}>
				<View style={styles.paddingTop5}>
					<FeedUser isSmall={true} size={35} />
				</View>
				<View style={styles.allumniCon}>
					<Image source={require('../../img/allumni.png')} style={styles.allmniIcon} />
					{this.props.data && this.props.data.alumni && <Text style={styles.allumniConText}>{this.props.data.alumni} college allumni work here</Text>}
				</View>
				<Text style={styles.borderBox}></Text>
				<TouchableOpacity style={styles.centered}>
					<Text style={styles.followLink}>View Job</Text>
				</TouchableOpacity>
			</View>
		)
	}
}