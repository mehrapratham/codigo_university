import React from 'react'
import { View, Text, Image, TouchableOpacity } from 'react-native'
import styles from '../../styles/styles'

export default class MultiImages extends React.Component{
	render(){
		return(
			<View style={styles.multiImageCon}>
        <View style={styles.multiImgBox}>
          <Image source={require('../../img/multiImg.png')} style={styles.feedImg} />
        </View>
        <View style={styles.multiImgBox}>
          <Image source={require('../../img/multiImg.png')} style={styles.feedImg} />
        </View>
        <View style={styles.multiImgBox}>
          <Image source={require('../../img/multiImg.png')} style={styles.feedImg} />
        </View>
        <View style={styles.multiImgBox}>
          <Image source={require('../../img/multiImg.png')} style={styles.feedImg} />
          <TouchableOpacity style={styles.haveMoreImg} activeOpacity={0.8}>
            <Text style={styles.moreText}>+12</Text>
          </TouchableOpacity>
        </View>
      </View>
		)
	}
}