import React, { Component } from 'react';
import { View, Text, Image, TouchableOpacity, ScrollView } from 'react-native'
import styles from '../../../styles/styles'
import { Tabs, Tab, Icon } from 'native-base';
import GradientButton from '../../Form/GradientButton'
export default class EventCollegesList extends Component {
  render() {
    return (
      <TouchableOpacity activeOpacity={0.7} style={styles.outerConCollege}>
        <View style={styles.relativeConCollege}>
          <View style={styles.absoluteInnerConCollege}>
            <Image source={require('../../../img/followUni.png')} style={styles.replyImg}/>
          </View>
          <View style={styles.innerViewCollg}>
            <Text style={styles.maniiPalText}>Manipal University</Text>
            <Text style={styles.manipalText2}>Manipal,Karnataka,India</Text>
          </View>
          <View style={styles.absoluteInnerSecondConCollege}>
            <GradientButton label={'FOLLOW'} smallGradientButtn={styles.followGradientBtn} smallGradientText={styles.followGradientText}/>
          </View>
        </View>
      </TouchableOpacity>
    );
  }
}
