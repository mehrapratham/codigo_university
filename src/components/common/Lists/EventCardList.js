import React, { Component } from 'react';
import { View, Text, Image, TouchableOpacity, ScrollView } from 'react-native'
import styles from '../../../styles/styles'
import { Tabs, Tab, Icon } from 'native-base';
export default class EventCardList extends Component {
  render() {
    return (
      <TouchableOpacity style={styles.mainEventView} activeOpacity={0.7}>
        <View style={styles.absoluteViewEvent}>
          <Image source={require('../../../img/management.jpeg')} style={styles.replyImg}/>
        </View>
        <View style={styles.marginRight10}>
          <View style={styles.relativeViewEvent}>
            <View>
              <Text style={styles.eventManagement} numberOfLines={1}>Marketing and Management</Text>
            </View>
            <View style={styles.viewAbsoluteEvent}>
              <TouchableOpacity activeOpacity={0.7}>
                <Icon name="arrow-forward" style={styles.forwardIcon}/>
              </TouchableOpacity>
            </View>
          </View>
          <Text numberOfLines={1} style={[styles.fontSize12Grey,styles.marginBottom7]}>17-19th May, Fri-Sun</Text>
          <Text numberOfLines={1} style={[styles.fontSize12Grey,styles.marginBottom7]}>Dr. TMA Pai Hall,Manipal University</Text>
          <Text numberOfLines={1} style={[styles.fontSize12Grey,styles.marginBottom7]}>Skill-Design,Marketing,Resource</Text>
          <Text style={[styles.fontSize12DarkGrey]}>Free Event</Text>
        </View>
      </TouchableOpacity>
    );
  }
}
