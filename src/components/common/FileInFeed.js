import React from 'react'
import { View, Text, Image, TouchableOpacity } from 'react-native'
import {Icon} from 'native-base'
import styles from '../../styles/styles'

export default class FileInFeed extends React.Component{
	render(){
		return(
			<View style={[styles.hasPadding15, styles.marginTopMinus20]}>
        <View style={styles.fileFeedCon}>
          <View style={styles.fileNameCon}>
            <Text style={styles.fileName}>Engineering.....pdf</Text>
            <Text style={styles.fileSize}>12 MB</Text>
          </View>
          <Image source={require('../../img/pdf.png')} style={styles.fileIcon} />
          <TouchableOpacity style={styles.downloadIcon}>
            <Image source={require('../../img/download.png')} />
          </TouchableOpacity>
        </View>
      </View>
		)
	}
}