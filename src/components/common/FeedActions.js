import React from 'react'
import { View, Text, TouchableOpacity, Image } from 'react-native'
import { Icon } from 'native-base'
import styles from '../../styles/styles'
import { LinearGradient } from 'expo';

export default class FeedActions extends React.Component{
	render(){
		return(
			<View style={[styles.feedActionCon, this.props.noPadding && styles.noPadding]}>
				<View style={styles.actionsField}> 
					<TouchableOpacity onPress={this.props.onLikePress}>
						<Text style={styles.actionTextCon}>
							<Image source={require('../../img/like.png')} style={styles.actionImg}/>
							<Text style={styles.actionText}>100 Likes</Text>
						</Text>
					</TouchableOpacity>
				</View>
				<View style={[styles.actionsField, styles.centered]}>
					<TouchableOpacity>
						<Text style={styles.actionTextCon}> 
							<Image source={require('../../img/comment.png')} style={styles.actionImg}/>
							<Text style={styles.actionText}>8 Comment</Text>
						</Text>
					</TouchableOpacity>
				</View>
				<View style={[styles.actionsField, styles.rightAligned]}> 
					<TouchableOpacity>
						<Text style={[styles.actionTextCon, styles.paddingLeft25]}>
							<Image source={require('../../img/share.png')} style={styles.actionImg}/>
							<Text style={styles.actionText}>Share</Text>
						</Text>
					</TouchableOpacity>
				</View>
			</View>
		)
	}
}