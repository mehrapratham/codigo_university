import React from 'react'
import { View, Text, TouchableOpacity } from 'react-native'
import { List } from 'native-base';
import styles from '../../styles/styles'
import LikesList from '../common/LikesList'

export default class SearchMember extends React.Component{
  constructor(props){
    super(props);
    this.state = {
      list: [
        {
          name: 'Marion Moore',
          text: 'Student-NITK',
          button: 'CHAT',
          isFollow: false
        },
        {
          name: 'Marion Moore',
          text: 'Student-NITK',
          button: 'CHAT',
          isFollow: false
        },
        {
          name: 'Marion Moore',
          text: 'Student-NITK',
          button: 'FOLLOWING',
          isFollow: false
        },
        {
          name: 'Marion Moore',
          text: 'Student-NITK',
          button: 'FOLLOW',
          isFollow: false
        },
        {
          name: 'Marion Moore',
          text: 'Student-NITK',
          button: 'FOLLOW',
          isFollow: true
        },
        {
          name: 'Marion Moore',
          text: 'Student-NITK',
          button: 'FOLLOWING',
          isFollow: false
        }
      ]
    }
  }
  render(){
    return(
        <View>
          <View style={[styles.searchHeading, {backgroundColor: '#f6f7f8'}]}>
            <Text style={styles.headingTitle}>{this.props.title}</Text>
            <TouchableOpacity style={styles.clearSearch}>
              <Text style={styles.clearSearchLink}>{this.props.rightText}</Text>
            </TouchableOpacity>
          </View>
          <View style={{backgroundColor: '#e8e8ea', marginBottom: 10}}>
            <List>
              {this.state.list.map((item,key) => {
                return(
                  <LikesList list={item} key={key} /> 
                  )
                })
              }
            </List>
          </View>
        </View>
    )
  }
}
