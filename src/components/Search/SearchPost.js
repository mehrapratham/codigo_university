import React from 'react'
import { View, Text, TouchableOpacity, Image, Dimensions, TextInput, ScrollView } from 'react-native'
import { Container, Content, Tabs, Tab } from 'native-base';
import styles from '../../styles/styles'
import FeedSearchBox from '../../components/Form/FeedSearchBox'
import AddComment from '../common/AddComment'
import FeedUser from '../common/FeedUser'
import FeedActions from '../common/FeedActions'
import FileInFeed from '../common/FileInFeed'
import {Video} from 'expo'
import VideoPlayer from '@expo/videoplayer';

export default class SearchPost extends React.Component{
  render(){
    const { height,width } = Dimensions.get('window');
    return(
        <View>
          <View style={[styles.searchHeading, {backgroundColor: '#f6f7f8'}]}>
            <Text style={styles.headingTitle}>Posts from Followers</Text>
            <TouchableOpacity style={styles.clearSearch}>
              <Text style={styles.clearSearchLink}>See All</Text>
            </TouchableOpacity>
          </View>
          <View style={{backgroundColor: '#171b25'}}>
            <View style={styles.feedCon}>
              <View style={styles.feedHeader}>
                <View style={styles.marginBottom20}>
                  <FeedUser />
                </View>
                <View>
                  <TouchableOpacity>
                    <Text><Text style={styles.userfeedColor}>MRI-Safe Robots treat Epilepsy: </Text><Text style={styles.userfeedLink}>http://www.google.com reminds me of this.</Text></Text>
                  </TouchableOpacity>
                </View>
              </View>
              <TouchableOpacity onPress={() => {Actions.FeedDetail()}} activeOpacity={0.5}>
                    <Image source={require('../../img/feed.png')} style={styles.feedImg} />
              </TouchableOpacity>
              <View>
                <FeedActions onLikePress={() => {Actions.Likes()}}/>
              </View>
              <View>
                <AddComment />
              </View>
            </View>
            <View style={styles.feedCon}>
              <View style={[styles.feedHeader]}>
                <View style={styles.marginBottom20}>
                  <FeedUser />
                </View>
                <View>
                  <TouchableOpacity>
                    <Text><Text style={styles.userfeedColor}>MRI-Safe Robots treat Epilepsy:</Text> <Text style={styles.userfeedLink}>http://www.google.com reminds me of this.</Text></Text>
                  </TouchableOpacity>
                </View>
              </View>
              <View>
                <FileInFeed />
              </View>
              <View>
                <FeedActions />
              </View>
              <View>
                <AddComment />
              </View>
            </View>
            <View style={styles.feedCon}>
              <View style={styles.feedHeader}>
                <View style={styles.marginBottom20}>
                  <FeedUser />
                </View>
                <View>
                  <Text style={styles.videoText}>This Video explain fluid statics and dynamics in detail.</Text>
                </View>
              </View>
              <View>
                <VideoPlayer
                  videoProps={{
                    shouldPlay: false,
                    resizeMode: Video.RESIZE_MODE_CONTAIN,
                    source: {
                      uri: 'https://bitdash-a.akamaihd.net/content/sintel/hls/playlist.m3u8',
                    },
                  }}
                  isPortrait={true}
                  playFromPositionMillis={0}
                />
              </View>
              <View>
                <FeedActions />
              </View>
              <View>
                <AddComment />
              </View>
            </View>
          </View>
        </View>
    )
  }
}
