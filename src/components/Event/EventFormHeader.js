import React from 'react'
import { View, Text, Image, TouchableOpacity, ScrollView } from 'react-native'
import styles from '../../styles/styles'
import { LinearGradient } from 'expo';

export default class EventFormHeader extends React.Component{
	constructor(props){
		super(props)
		this.state = {
			steps: ['Create Event', 'Add Media', 'Add Members', 'FAQs']
		}
	}
	render(){
		let activeProgress = (100/4)
    activeProgress = activeProgress*this.props.step + '%'
		return(
			<View style={styles.eventHeader}>
				<Text style={styles.eventHeaderText}>Event</Text>
				<ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
					<View style={styles.eventStepsCon}>
						{this.state.steps.map((item, index)=>{
							return 	<TouchableOpacity style={[styles.eventStepBtn, (this.props.step == index+1) && styles.currentStep, (this.props.step > index+1) && styles.activeStep]}>
												<Text style={[styles.eventStepBtnText, (this.props.step == index+1) && styles.currentStepText, (this.props.step > index+1) && styles.activeStepText]}>{item}</Text>
											</TouchableOpacity>
						})}
					</View>
				</ScrollView>
				<View style={[styles.progressBar, styles.eventProcessCon]}>
	        <View style={[styles.progressBarActive, styles.eventProgressBar,{width: activeProgress}]}>
	        	<LinearGradient
		          colors={['#ef7176', '#f5ae5f']}
		          style={[styles.gradientBtn, {borderRadius: 0}]}
		          start={[0.1, 1.0]} end={[1.3, 0.3]}
		        >
		        </LinearGradient>
	        </View>
	      </View>
			</View>
		)
	}
}